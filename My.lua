my = {}

--paths
my.sheetPath = 'assets.art.sheets.'
my.gameMVCPath = 'game.gameMVC.'


--image paths

--sounds
my.audioBD = 'assets/audio/'
my.audioInfo = require('settings.AudioInfo')
my.audioExtension = "mp3"--"wav" -- "m4a"

--system awareness
my.isSimulator = system.getInfo( "environment" ) == "simulator"

--layout based on corona
my.centerX = display.screenOriginX + display.actualContentWidth/2
my.centerY = display.screenOriginY + display.actualContentHeight/2
my.width = display.actualContentWidth -- the width of the screen adjusted after content scaling.
my.height = display.actualContentHeight
--these are relative to the screen.
my.bottom =  display.actualContentHeight/2
my.top    =  -display.actualContentHeight/2
my.left   =  -display.actualContentWidth/2
my.right  =  display.actualContentWidth/2

-- my.startupFetch = {
--     key = "1V5ElwmiCw_yc-U7aaWoduA-TWHoGMdNQoUXEYN4ZpV4",
--     sheets = {
--         {title = "presidents"},
--         {title = "periodic"},
--     },
-- }

my.configData = {} -- where all the gdoc info is stored.

display.setDefault("anchorX", .5)
display.setDefault("anchorY", .5)

--notifications.

--fonts.
--my.standardFont = "MiniPixel-7"--ArialRoundedMTBold" --"SnellRoundhand-Black"--"Helvetica"

my.prepareGame = function(params)

	local physics = require "physics"
	physics.start()
    -- display.setDefault( "minTextureFilter", "nearest" )
    -- display.setDefault( "magTextureFilter", "nearest" )
    -- PushwooshAdapter:init()
    -- MultitouchTracker:init()
	-- print("CAME HERE")
	-- local gdoc = GoogleDoc:new()
    -- gdoc:read{doc = my.levelDocInfo,
 --              onComplete = onDone,
 --             }

     -- get all the levels from gdocs, then start the game.

    Director:changeScene(params)


end
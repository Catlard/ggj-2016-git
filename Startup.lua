local Startup = {}

function Startup:init(callback)
	local startup = {}
	local DataLoader = require('settings.DataLoader')
	local resumes
	local fetches = {
		--function() DataLoader:init(startup, my.startupFetch) end,
	}

	function startup:start()
		print("Startup: Running...")
		resumes = 0
		if #fetches == 0 then
			self:finish()
		else
			for i = 1, #fetches do 
				fetches[i]()
			end
		end
	end

	function startup:finish()
		print("Startup: Finished!")
		DataLoader = nil
		callback()
	end

	function startup:resume(fetchName)
		resumes = resumes + 1
		print("Startup: Completed " .. fetchName)
		if resumes == #fetches then self:finish() end
	end

	

	startup:start()
end

return Startup
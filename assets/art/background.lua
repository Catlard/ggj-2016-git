--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:f7001d803f80609a0f9d72f41ca18e3c:9ffeb00ff1eee2d055464b4a6d5e7f0e:95293590400590f179a4981466390727$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- 1
            x=1,
            y=1,
            width=1280,
            height=720,

        },
        {
            -- 2
            x=1,
            y=723,
            width=1280,
            height=720,

        },
    },
    
    sheetContentWidth = 1282,
    sheetContentHeight = 1444
}

SheetInfo.frameIndex =
{

    ["1"] = 1,
    ["2"] = 2,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo

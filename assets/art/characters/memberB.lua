--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:4fa0db66ed8a529ac7ece168471fd554:d1a07ced8ef641f20cef55d53ea64901:0bc5002eaf39ea934fb40df0d0a332e1$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- B_body
            x=1422,
            y=319,
            width=359,
            height=507,

        },
        {
            -- B_head
            x=1,
            y=1,
            width=607,
            height=629,

        },
        {
            -- B_lArm
            x=1255,
            y=1,
            width=555,
            height=157,

        },
        {
            -- B_lArm_grow
            x=1,
            y=632,
            width=625,
            height=227,

        },
        {
            -- B_lLeg
            x=1255,
            y=319,
            width=165,
            height=531,

        },
        {
            -- B_lLeg_grow
            x=849,
            y=1,
            width=235,
            height=605,

        },
        {
            -- B_rArm
            x=1255,
            y=160,
            width=549,
            height=157,

        },
        {
            -- B_rArm_grow
            x=628,
            y=610,
            width=619,
            height=227,

        },
        {
            -- B_rLeg
            x=1086,
            y=1,
            width=167,
            height=535,

        },
        {
            -- B_rLeg_grow
            x=610,
            y=1,
            width=237,
            height=607,

        },
    },
    
    sheetContentWidth = 1877,
    sheetContentHeight = 860
}

SheetInfo.frameIndex =
{

    ["B_body"] = 1,
    ["B_head"] = 2,
    ["B_lArm"] = 3,
    ["B_lArm_grow"] = 4,
    ["B_lLeg"] = 5,
    ["B_lLeg_grow"] = 6,
    ["B_rArm"] = 7,
    ["B_rArm_grow"] = 8,
    ["B_rLeg"] = 9,
    ["B_rLeg_grow"] = 10,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo

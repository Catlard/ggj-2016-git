--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:65cdb21f504ffe7cfdeb308bc98d3462:1c61ec7857f2362e90730f81b24b69f8:49f9f881d7b32508a26101cf07d8920f$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- king_body
            x=1350,
            y=1,
            width=483,
            height=567,

        },
        {
            -- king_head
            x=1,
            y=1,
            width=797,
            height=849,

        },
        {
            -- king_lArm
            x=1,
            y=1055,
            width=697,
            height=133,

        },
        {
            -- king_lArm_grow
            x=1,
            y=852,
            width=765,
            height=201,

        },
        {
            -- king_lLeg
            x=1535,
            y=570,
            width=211,
            height=679,

        },
        {
            -- king_lLeg_grow
            x=800,
            y=1,
            width=273,
            height=741,

        },
        {
            -- king_rArm
            x=700,
            y=1055,
            width=697,
            height=131,

        },
        {
            -- king_rArm_grow
            x=768,
            y=852,
            width=765,
            height=201,

        },
        {
            -- king_rLeg
            x=1748,
            y=570,
            width=211,
            height=679,

        },
        {
            -- king_rLeg_grow
            x=1075,
            y=1,
            width=273,
            height=741,

        },
    },
    
    sheetContentWidth = 1960,
    sheetContentHeight = 1250
}

SheetInfo.frameIndex =
{

    ["king_body"] = 1,
    ["king_head"] = 2,
    ["king_lArm"] = 3,
    ["king_lArm_grow"] = 4,
    ["king_lLeg"] = 5,
    ["king_lLeg_grow"] = 6,
    ["king_rArm"] = 7,
    ["king_rArm_grow"] = 8,
    ["king_rLeg"] = 9,
    ["king_rLeg_grow"] = 10,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo

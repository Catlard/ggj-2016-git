--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:0cc9dcd83abb5c7feb83d6eb882e3434:3fd4b4e242675f96ee2d67e5a3b449ad:48429e8e6b1e8fa9a044e04e42ce70f4$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- help01
            x=1,
            y=1,
            width=446,
            height=408,

        },
        {
            -- help02
            x=449,
            y=1,
            width=446,
            height=392,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 446,
            sourceHeight = 408
        },
    },
    
    sheetContentWidth = 896,
    sheetContentHeight = 410
}

SheetInfo.frameIndex =
{

    ["help01"] = 1,
    ["help02"] = 2,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo

--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:d2f887724e6171ecff7d7861bb8e1b45:a49e8d7991bf197fb476483bc39451e9:ce6a725bf1a2f3bd2792d8df4e48bcf5$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- king01
            x=1,
            y=1,
            width=691,
            height=660,

        },
        {
            -- king02
            x=694,
            y=1,
            width=691,
            height=660,

        },
    },
    
    sheetContentWidth = 1386,
    sheetContentHeight = 662
}

SheetInfo.frameIndex =
{

    ["king01"] = 1,
    ["king02"] = 2,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo

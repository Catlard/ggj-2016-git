--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:5adfa111c9a2109b07a1d6b5287df54a:65a4e2a4d63156b3a141f33819efd2fd:ecc872e1b761b9ed654bb49d02e2458a$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- num_1
            x=1,
            y=1,
            width=182,
            height=160,

        },
        {
            -- num_2
            x=1,
            y=163,
            width=182,
            height=160,

        },
        {
            -- num_3
            x=1,
            y=325,
            width=182,
            height=160,

        },
        {
            -- num_4
            x=1,
            y=487,
            width=182,
            height=160,

        },
        {
            -- num_5
            x=1,
            y=649,
            width=182,
            height=160,

        },
        {
            -- num_stop
            x=1,
            y=811,
            width=182,
            height=160,

        },
    },
    
    sheetContentWidth = 184,
    sheetContentHeight = 972
}

SheetInfo.frameIndex =
{

    ["num_1"] = 1,
    ["num_2"] = 2,
    ["num_3"] = 3,
    ["num_4"] = 4,
    ["num_5"] = 5,
    ["num_stop"] = 6,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo

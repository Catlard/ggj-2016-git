--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:9792ea2e1eac99fbd076abbe0c3223bf:490ec2a4b0e62a818fc04124da2eab9c:8fb2bdd024bc4ff41a4921d6dbb03750$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- people_1
            x=1,
            y=1,
            width=148,
            height=212,

            sourceX = 3,
            sourceY = 21,
            sourceWidth = 160,
            sourceHeight = 240
        },
        {
            -- people_2
            x=151,
            y=1,
            width=142,
            height=148,

            sourceX = 4,
            sourceY = 85,
            sourceWidth = 160,
            sourceHeight = 240
        },
    },
    
    sheetContentWidth = 294,
    sheetContentHeight = 214
}

SheetInfo.frameIndex =
{

    ["people_1"] = 1,
    ["people_2"] = 2,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo

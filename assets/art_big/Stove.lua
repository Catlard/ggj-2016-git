--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:e4655bcd0293a3d4d47c0ade8475de2f:bec0e02fd95e58221389b4ead3a18bcb:1190ba740bf51deb973489a3c61567da$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- stove_1
            x=1,
            y=1,
            width=201,
            height=376,

        },
        {
            -- stove_2
            x=1,
            y=379,
            width=201,
            height=374,

            sourceX = 0,
            sourceY = 2,
            sourceWidth = 201,
            sourceHeight = 376
        },
    },
    
    sheetContentWidth = 203,
    sheetContentHeight = 754
}

SheetInfo.frameIndex =
{

    ["stove_1"] = 1,
    ["stove_2"] = 2,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo

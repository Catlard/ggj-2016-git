--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:986cdcdb8e5f09a5872cc306073d2724:23fd880f2c160486771d448ed76e428a:8e6286f7aab2b4b8ef339642ac7e7391$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- bg_floor_1
            x=803,
            y=1,
            width=796,
            height=356,

            sourceX = 0,
            sourceY = 24,
            sourceWidth = 800,
            sourceHeight = 380
        },
        {
            -- bg_floor_2
            x=1,
            y=1,
            width=800,
            height=356,

            sourceX = 0,
            sourceY = 24,
            sourceWidth = 800,
            sourceHeight = 380
        },
    },
    
    sheetContentWidth = 1600,
    sheetContentHeight = 358
}

SheetInfo.frameIndex =
{

    ["bg_floor_1"] = 1,
    ["bg_floor_2"] = 2,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo

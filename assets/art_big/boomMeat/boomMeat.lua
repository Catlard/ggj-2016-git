--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:04f5e09dc9ff0b2d2a19b540d0f5128d:22bb71be317c6be71b81095216ce5314:75a67a741246df7455df5300cd2a758e$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- boom01
            x=303,
            y=1,
            width=290,
            height=232,

            sourceX = 4,
            sourceY = 115,
            sourceWidth = 300,
            sourceHeight = 356
        },
        {
            -- boom02
            x=1,
            y=1,
            width=300,
            height=242,

            sourceX = 0,
            sourceY = 109,
            sourceWidth = 300,
            sourceHeight = 356
        },
        {
            -- boom03
            x=595,
            y=1,
            width=250,
            height=208,

            sourceX = 28,
            sourceY = 115,
            sourceWidth = 300,
            sourceHeight = 356
        },
        {
            -- boom04
            x=847,
            y=1,
            width=250,
            height=154,

            sourceX = 28,
            sourceY = 169,
            sourceWidth = 300,
            sourceHeight = 356
        },
    },
    
    sheetContentWidth = 1098,
    sheetContentHeight = 244
}

SheetInfo.frameIndex =
{

    ["boom01"] = 1,
    ["boom02"] = 2,
    ["boom03"] = 3,
    ["boom04"] = 4,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo

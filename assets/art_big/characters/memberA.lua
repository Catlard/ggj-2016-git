--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:ab21b43b2ba0d69bd2d727e7b3de7945:0169d632a2b4812bb0b29c43e2049d22:bd458b614b0cfbccf67dc4252cc2791d$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- A_body
            x=1,
            y=236,
            width=409,
            height=517,

        },
        {
            -- A_head
            x=620,
            y=1,
            width=589,
            height=785,

        },
        {
            -- A_lArm
            x=1,
            y=755,
            width=547,
            height=165,

        },
        {
            -- A_lArm_grow
            x=1,
            y=1,
            width=617,
            height=233,

        },
        {
            -- A_lLeg
            x=412,
            y=236,
            width=189,
            height=513,

        },
        {
            -- A_lLeg_grow
            x=1211,
            y=1,
            width=255,
            height=583,

        },
        {
            -- A_rArm
            x=550,
            y=788,
            width=547,
            height=165,

        },
        {
            -- A_rArm_grow
            x=1211,
            y=586,
            width=617,
            height=233,

        },
        {
            -- A_rLeg
            x=1725,
            y=1,
            width=189,
            height=513,

        },
        {
            -- A_rLeg_grow
            x=1468,
            y=1,
            width=255,
            height=583,

        },
    },
    
    sheetContentWidth = 1915,
    sheetContentHeight = 987
}

SheetInfo.frameIndex =
{

    ["A_body"] = 1,
    ["A_head"] = 2,
    ["A_lArm"] = 3,
    ["A_lArm_grow"] = 4,
    ["A_lLeg"] = 5,
    ["A_lLeg_grow"] = 6,
    ["A_rArm"] = 7,
    ["A_rArm_grow"] = 8,
    ["A_rLeg"] = 9,
    ["A_rLeg_grow"] = 10,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo

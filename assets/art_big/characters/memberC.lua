--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:ed2813f4ef00463b0d6c36e8bfb2cf44:cb77a0c280eabf2bed9e2cfd86ec3b48:06af207dad0429ca42911f125fb0ab7d$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- C_body
            x=631,
            y=477,
            width=453,
            height=627,

        },
        {
            -- C_head
            x=388,
            y=1146,
            width=887,
            height=791,

        },
        {
            -- C_lArm
            x=1,
            y=1,
            width=671,
            height=201,

        },
        {
            -- C_lArm_grow
            x=1,
            y=204,
            width=743,
            height=271,

        },
        {
            -- C_lLeg
            x=316,
            y=477,
            width=313,
            height=599,

        },
        {
            -- C_lLeg_grow
            x=1,
            y=1078,
            width=385,
            height=671,

        },
        {
            -- C_rArm
            x=674,
            y=1,
            width=685,
            height=201,

        },
        {
            -- C_rArm_grow
            x=746,
            y=204,
            width=755,
            height=271,

        },
        {
            -- C_rLeg
            x=1,
            y=477,
            width=313,
            height=595,

        },
        {
            -- C_rLeg_grow
            x=1086,
            y=477,
            width=385,
            height=667,

        },
    },
    
    sheetContentWidth = 1502,
    sheetContentHeight = 1938
}

SheetInfo.frameIndex =
{

    ["C_body"] = 1,
    ["C_head"] = 2,
    ["C_lArm"] = 3,
    ["C_lArm_grow"] = 4,
    ["C_lLeg"] = 5,
    ["C_lLeg_grow"] = 6,
    ["C_rArm"] = 7,
    ["C_rArm_grow"] = 8,
    ["C_rLeg"] = 9,
    ["C_rLeg_grow"] = 10,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo

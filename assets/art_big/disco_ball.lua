--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:0d362d21f601d4efdfd6bf99199e97bf:09d2a64b6d06803afef1735d6fcf2cbd:65b5d75c079a23229fc2da65fba6a089$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- dragon_V10
            x=571,
            y=1,
            width=172,
            height=186,

            sourceX = 23,
            sourceY = 0,
            sourceWidth = 230,
            sourceHeight = 230
        },
        {
            -- dragon_V11
            x=745,
            y=1,
            width=158,
            height=184,

            sourceX = 32,
            sourceY = 0,
            sourceWidth = 230,
            sourceHeight = 230
        },
        {
            -- dragon_V12
            x=1,
            y=1,
            width=188,
            height=210,

            sourceX = 12,
            sourceY = 0,
            sourceWidth = 230,
            sourceHeight = 230
        },
        {
            -- dragon_V13
            x=191,
            y=1,
            width=188,
            height=210,

            sourceX = 27,
            sourceY = 0,
            sourceWidth = 230,
            sourceHeight = 230
        },
        {
            -- dragon_V14
            x=381,
            y=1,
            width=188,
            height=210,

            sourceX = 22,
            sourceY = 0,
            sourceWidth = 230,
            sourceHeight = 230
        },
    },
    
    sheetContentWidth = 904,
    sheetContentHeight = 212
}

SheetInfo.frameIndex =
{

    ["dragon_V10"] = 1,
    ["dragon_V11"] = 2,
    ["dragon_V12"] = 3,
    ["dragon_V13"] = 4,
    ["dragon_V14"] = 5,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    dump(self.frameIndex)
    return self.frameIndex[name];
end

return SheetInfo

--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:77f7959a5ee0ca4bed4e45a8043bf753:8ba588836fda7a32e3373f0a57ab5a49:a35a56c30fc76068cbf822f81b7f542d$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- pot01
            x=1,
            y=1,
            width=414,
            height=289,

        },
        {
            -- pot02
            x=1,
            y=292,
            width=414,
            height=289,

        },
        {
            -- pot03
            x=1,
            y=583,
            width=414,
            height=289,

        },
    },
    
    sheetContentWidth = 416,
    sheetContentHeight = 873
}

SheetInfo.frameIndex =
{

    ["pot01"] = 1,
    ["pot02"] = 2,
    ["pot03"] = 3,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo

local setup = {}

function setup:init()
	setup:corona()

	setup:tools('kt.system.tools.')
	setup:sprites('kt.system.sprites.')
	setup:display('kt.system.display.')
	setup:debug('kt.system.debug.')
	setup:services('kt.system.services.')
	setup:audio('kt.system.audio.')
	setup:context('kt.system.context.')
	setup:io('kt.system.io.')

	setup:notifications('kt.components.notifications.')
	setup:ui('kt.components.ui.')
	setup:touch('kt.components.touch.')


end

function setup:corona()
    math.randomseed(os.time())
    system.activate( "multitouch" )
    display.setStatusBar(display.HiddenStatusBar)
    require("kt.Globals")
end

function setup:io(path)
	PickleTable = require(path.."PickleTable")
	KittenIO = require(path.."KittenIO")

end

function setup:notifications(path)
	PushwooshAdapter = require(path.."PushwooshAdapter")
end

function setup:touch(path)
	MultitouchTracker = require(path.."MultitouchTracker"):new()
	MultitouchFilter = require(path.."MultitouchFilter") -- a mixin.
end

function setup:context(path)
	Director = require(path..'Director'):new()
	MVCContext = require(path..'MVCContext')
end

function setup:ui(path)
	ProgressCircle = require(path..'ProgressCircle')
	Button = require(path..'Button')
end

function setup:audio(path)
	SFX = require(path..'SFX')
end

function setup:services(path)
	GoogleDocFetcher     = require(path..'GoogleDocFetcher'):new()
	DB           		 = require(path..'DB'):new()
	SafariView           = require(path..'SafariView')
end

function setup:debug(path)
	DebugDisplay = require(path.."DebugDisplay")
	require(path.."DataDumper")
	Console = require(path.."Console"):new()
end

function setup:tools(path)
	require(path.."Strings")
	require(path.."Tables")
	require(path.."Math")
	require(path.."Debug")
	require(path.."CleanGroup")
	Canceler = require(path.."Canceler")
	Colors = require(path.."Colors")
	Trigger = require(path.."Trigger"):new()
	Tween = require(path.."Tween"):new()
	Stopwatch = require(path.."Stopwatch")
end

function setup:display(path)
	Screen        = require(path.."Screen"):new()
	IsometricGrid = require(path.."IsometricGrid")
	Device        = require(path.."DeviceIdentifier"):new()
end

function setup:sprites(path)
	SpriteSheet = require(path.."SpriteSheet")
	Sheets      = require(path.."Sheets"):new()
end

setup:init()

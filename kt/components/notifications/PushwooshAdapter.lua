local PushwooshAdapter = {}

function PushwooshAdapter:init()
	--pushwoosh stuff.
	
	local pushwoosh = require( "kt.components.notifications.Pushwoosh" )
	pushwoosh.registerForPushNotifications( my.pushwooshID, launchArgs ) -- specify your application Id
	Runtime:addEventListener( "pushwoosh-notification", function(event) 
	    local site = event.data.custom.pw.l
	    print("On Reception of pushwoosh event, went to:", site)
	    local safariView = SafariView:new{website = site}
	end)



end

return PushwooshAdapter
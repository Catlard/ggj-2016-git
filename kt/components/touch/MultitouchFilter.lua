local Filter = {}

function Filter:new(showMessage)
	showMessage = showMessage or function() end
	local filter = {}
	local numTouches = 0
	local openTouchTime = 25
	local touchJudged = false
	local touchTrigger

	local function killTrigger()
		if touchTrigger then touchTrigger:cancel(); touchTrigger = nil end
	end

	function filter:reset()
		killTrigger()
		numTouches = 0
	end

	function filter:onTouch(event, listener)
		local function waitToJudgeTouch()
			killTrigger()
			touchTrigger = Trigger.create(openTouchTime, function()
				touchJudged = true
				local multiPhase = string.format("%sFingerTap", numTouches)
				listener{phase = multiPhase}
				showMessage(multiPhase)
			end)
		end

		if event.phase == "began" then
			numTouches = numTouches + 1
			--showMessage(numTouches .. " STARTED ")
			touchJudged = false
			showMessage("")
			waitToJudgeTouch()

		elseif event.phase == "ended" or 
			   event.phase == "cancelled" or 
			   event.phase == "rolledOff" then
			numTouches = numTouches -1
			if numTouches < 0 then numTouches = 0 end
			killTrigger()
			touchJudged = false
			--showMessage(numTouches .. " ENDED ")
		end
		return true
	end

	function filter:destroy()
		killTrigger()
		filter = nil
	end

	return filter
end

return Filter


local MultiTouchTracker = {}

function MultiTouchTracker:new()
	local tracker = {}
	tracker.touchCount = 0
	local inputCollector
	local objsToWatch = {}

	local function sendEvent(event, newInfo, phase)
		local e = copy(event)
		e.target = newInfo.obj
		if phase then e.phase = phase end
		newInfo.listener(e)
	end

	local function manageTouchCount(phase)
		if phase == "began" then
			tracker.touchCount = tracker.touchCount + 1
		elseif phase == "ended" then
			tracker.touchCount = tracker.touchCount - 1
		end
	end

	function onTouch(event)
		local phase = event.phase
		manageTouchCount(phase ~= "moved" and phase)

		for i = 1, #objsToWatch do
			local thisID = tostring(event.id)
			local info = objsToWatch[i]
			local inBounds = math.inBounds(event.x, event.y, info.obj.contentBounds)
			local tbi = info.touchesByID
			if phase == "began" and inBounds then
				table.insert(tbi, 
				{
					id = thisID,
					startedInside = true,
					inBounds = true
				})
				sendEvent(event, info)
			elseif phase == "moved" then
				for i = 1, #tbi do
					if tbi[i].inBounds and thisID == tbi[i].id then 
						if inBounds then sendEvent(event, info)
						else
							print("JUST ROLLED OFF!")
							tbi[i].inBounds = false
							sendEvent(event, info, "rolledOff")
						end
					end
				end
			elseif event.phase == "ended" and inBounds then
				for i = 1, #tbi do 
					if tbi[i].startedInside and thisID == tbi[i].id then
						tbi[i].startedInside = false
						tbi[i].inBounds = false
						sendEvent(event, info)
					end
				end
			end 
		end 
	end  

	function tracker:init()
		--create collector.
		inputCollector = display.newRect(0,0,my.width,my.height)
		inputCollector:setFillColor(1,1,1, .01)
		Screen.inputLayer:insert(inputCollector)
		inputCollector:addEventListener("touch", onTouch)
	end

	function tracker:watch(dispObj, listener)
		table.insert(objsToWatch, {
			obj = dispObj, 
			listener = listener,
			touchesByID = {}
		})
	end

	function tracker:remove(dispObj)
		for i = 1, #objsToWatch do
			if objsToWatch[i].obj == dispObj then
				table.remove(objsToWatch, i)
				return
			end
		end
		print("Could not remove object from multitouch tracker:")
		dump(dispObj)
	end

	function tracker:destroy()
		system.deactivate("multitouch")
		Runtime:removeEventListener("touch", touch)
		tracker = nil
	end

	return tracker

end


return MultiTouchTracker
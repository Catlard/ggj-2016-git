local Button = {}

function Button:new(params)
	
	params = params or {}
	
	--vars
	local rect, pic
	local bGroup = display.newGroup()
	local height = params.height or 40
	local width = params.width or 80
	local text = params.text
	local textColor = params.textColor or {1,1,1,1}
	local textScale = params.textScale or 1
	local textOptions = params.textOptions or 
	{
	    width = width,     --required for multi-line and alignment
	    font = my.standardFont,   
	    fontSize = 12,
	    align = "center"  --new alignment parameter
	}
	text = text or textOptions.text -- make sure it's in the text optiosn.
	textOptions.text = text

	local picLocation = params.picLocation
	local color = params.color or {0,0,.5,.5}
	
	--conditional setup.
	if not text and not picLocation then text = "Empty" end
	if params.parent then params.parent:insert(bGroup) end
	if params.table then table.insert(params.table, bGroup) end

	local function onTouch(event)
		if event.phase == "began" then
			if params.action then params.action() end
		end
	end	

	--set up button appearance.
	if picLocation then 
		rect = display.newImage(picLocation, true)
	else 
		rect = display.newRect(0,0,width,height) 
	end

	bGroup:insert(rect)
	rect:setFillColor(unpack(color))
	rect:addEventListener("touch", onTouch)


	if text then
		textOptions.width = textOptions.width or rect.contentWidth
		local t = display.newText(textOptions)
		t:setFillColor(unpack(textColor))
		t:scale(textScale,textScale)
		bGroup:insert(t)
	end

	function bGroup:destroy()
		if rect then rect:removeEventListener("touch", onTouch) end
		cleanGroup(bGroup)
		bGroup = nil
	end	


	return bGroup
end

return Button
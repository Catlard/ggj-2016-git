local ProgressCircle = {}

function ProgressCircle:new(params)
	local circleGroup = display.newGroup()
	circleGroup.rotation = 90
	circleGroup:scale(-1,1)

	params = params or {}
	
	local allCircles = {}
	local colorTween
	local colorTweenTime = params.tweenTime or 100
	local circleRadius = params.circleRadius or 5
	local numCircles = params.numCircles or 40
	local totalRadius = params.totalRadius or 50
	local fullColor = params.fullColor or {.6,.6,.6,1}
	local emptyColor = params.emptyColor or {.9,.9,.9,1}


	function circleGroup:init()

		local inc = 360/numCircles
		local isShowingLayer = false

	

		for i = 1, numCircles do
			local radians = math.rad (i * inc)
			local cx = math.cos(radians) * totalRadius
			local cy = math.sin(radians) * totalRadius
			local c = display.newCircle(cx,cy,-circleRadius)
			table.insert(allCircles, c)
			circleGroup:insert(c)
			c:setFillColor(unpack(fullColor))
		end 
	end

	function circleGroup:lerpColor(percent, a, b)
		local lerped = {}
		for i = 1, 4 do
			local iA, iB = a[i], b[i]
			if iB > iA then 
				iA, iB = iB, iA
			end
			table.insert(lerped, ((iA - iB) * percent) + iB)
		end
		return lerped
	end


	function circleGroup:setState(percent)
		if percent > 1 then percent = 1
		elseif percent < 0 then percent = 0 end

		local middleNode = math.ceil(percent * numCircles)

		for i = 1, numCircles do

			if i < middleNode then
				allCircles[i]:setFillColor(unpack(fullColor))
			elseif i == middleNode then
				local remainder = (middleNode - (percent * numCircles))/numCircles
				remainder = remainder/(1/numCircles)
				local lerpedColor = self:lerpColor(remainder, fullColor, emptyColor)
				allCircles[i]:setFillColor(unpack(lerpedColor))
			else
				allCircles[i]:setFillColor(unpack(emptyColor))
			end
		end


	end

	function circleGroup:destroy()
		allCircles = nil
		cleanGroup(circleGroup)
		circleGroup = nil


	end





	return circleGroup
end

return ProgressCircle
local SFX = {}

local extension = my.audioExtension
local sfxBD =   string.format("%s%s/", my.audioBD, extension)
local info = my.audioInfo

local function log(message)
	print(message)
	Console:add(message, "WARN")
end

function SFX:findInfo(name, options)
	local result = info[name]
	local denial

	if not result then
		denial = "SFX: No category named "..name.." exists in the AudioInfo file."
	elseif not result.name then --we choose one randomly.
		local ndx = math.random(#result)
		if options.playInOrder then
			ndx = result.lastPlayedSFX and result.lastPlayedSFX + 1 or 1
			if ndx > #result then ndx = 1 end
			result.lastPlayedSFX = ndx
		end
		result = result[ndx]
	end

	if result and not result.name then
		denial = "SFX: Your sound "..name.." either has no name, or is improperly formatted somehow in the AudioInfo file."
	end

	if denial then log(denial) 
	else
		
		return result 
	end
end

local function getRandWithinRange(range)
	return math.lerp(range[1], range[2], math.random())
end

local function prepareInfo(info)
	local r = copy(info)
	r.pitch = r.pitch or 1
	if type(r.pitch) == "table" then
		r.pitch = getRandWithinRange(r.pitch) end

	r.volume = r.volume or 1
	if type(r.volume) == "table" then
		r.volume = getRandWithinRange(r.volume) end


	return r

end

function SFX:play(infoName, options)

	--ways to play.
	options = options or {}

	if type(infoName) == "string" then
		local foundInfo = self:findInfo(infoName, options)
		if not foundInfo then
			 return
		else 
			preparedInfo = prepareInfo(foundInfo)
		end
	else
		preparedInfo = prepareInfo(infoName) -- it's info!
	end



	local fullName = string.format("%s%s.%s",sfxBD,preparedInfo.name,extension)

	local data = audio.loadSound(fullName)
	if not data then
		local message = "SFX: Could not find audio:"..fullName
		log(message) 
		local errorMessage = "SFX: "..tostring(infoName)
		return {stop = function() 
			log(errorMessage.." couldn't be stopped. (nil)")
		end}
	end
	
	local mychannel, mysource
	if not MUTE then
		mychannel, mysource = audio.play(data, options)
		audio.setVolume(preparedInfo.volume, {source= mysource})
		al.Source(mysource, al.PITCH, preparedInfo.pitch)
	else
		print("Would have played " .. fullName .. " but it was MUTED.")
	end

	return {
		stop = function()
			print("MY STOPPED CHANNEL WAS::::")
			dump(mychannel)
			audio.stop(mychannel)
		end,
		channel = mychannel,
		source = mysource
	}
end


return SFX
local Director = {}

function Director:new()

	local director = {}
	local canceler = Canceler:new()

	local scenes = {}
	local fadeScreen = display.newRect(0,0,my.width * 2, my.height * 2)
	Screen.topLayer = fadeScreen
	fadeScreen.x, fadeScreen.y = my.width/2,my.height/2
	fadeScreen.alpha = 0


	function director:changeScene(params)
		local newScene = params.scene
		params.fadeArgs = params.fadeArgs or {
			time = params.quick and 0 or 200,
			waitTime = params.quick and 0 or 100,
			wipeColor = {1,1,1,1}
		}
		fadeScreen:setFillColor(unpack(params.fadeArgs.wipeColor))

		canceler:add(Tween.create(fadeScreen, {
			alpha = 1,
			time = params.fadeArgs.time,
			onComplete = function() -- when fade screen is up.
				self:loadNewScene(params)
			end
		}))
	end


	function director:loadNewScene(params)
		table.insert(scenes, params)
		if Controller then 
			Controller:destroy()
			Controller = nil
		end
		require(params.scene)()
		MVCContext:orient()

		Controller:init(params.payload)
		Screen:add(View)
		canceler:add(Trigger.create(params.fadeArgs.waitTime, function()
			canceler:add(Tween.create(fadeScreen, {
				alpha = 0,
				time = params.fadeArgs.time
			}))
		end))
	end

	function director:destroy()
		display.remove(fadeScreen)
		canceler:destroy()
		canceler = nil
		fadeScreen = nil
	end


	return director

end


return Director
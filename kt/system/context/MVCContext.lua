local MVC = {}

local loggedViews

--when this is called, the Model, Controller, and View are all current, relevant objects.
function MVC:orient()
	if loggedViews then
		for className, view in pairs(loggedViews) do
			if view then
				print("MVC_CONTEXT: View ".. className .. " needed to be logged out.")
			end
		end
	end
	loggedViews = {}
end

function logViewIn(viewName, view)
	if loggedViews[viewName] then
		print("MVC_CONTEXT: Tried to log in a view with name " .. viewName .. ". One already existed.")
	elseif DEBUG_MVC_CONTEXT then
		print("MVC_CONTEXT: Created loggedView named " .. viewName)
	end
	loggedViews[viewName] = view
end

function logViewOut(viewName)
	if DEBUG_MVC_CONTEXT then
		print("MVC_CONTEXT: Removed loggedView named " .. viewName)
	end
	loggedViews[viewName] = nil
end

--runs functions in all views with a function named fName.
function tellViews(fName, fParams)
	if DEBUG_MVC_CONTEXT then
		print("MVC_CONTEXT: Telling views to " .. fName)
	end
	for className, view in pairs(loggedViews) do
		if loggedViews[className][fName] then
			if DEBUG_MVC_CONTEXT then
				print("MVC_CONTEXT:" .. TAB .."Found " .. fName .. " in " .. className)
			end
			loggedViews[className][fName](nil, fParams)
		end
	end
end




return MVC
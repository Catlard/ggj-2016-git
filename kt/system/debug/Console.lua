local Console = {}

function Console:new()
	local console = display.newGroup()
	local messageLayer = display.newGroup()
	console:insert(messageLayer)
	Screen.debugLayer:insert(console)
	local messages = {}

	--settings
	local updateDelay = 200
	local consoleWidth = my.width * .7
	local fontSize = 12
	local timeToStayPerChar = 100
	local minStayTime = 4000


	
	local severities = {
		GOOD = Colors.pastels.blue,
		OKAY = Colors.white,
		WARN = Colors.pastels.yellow,
		EROR = Colors.pastels.red,
		____ = Colors.gray
	}

	local function createMessageObj(height, data)
		--create message obj
		local color = data.color
		local mGroup = display.newGroup()
		local text = display.newText{
			text = data.message,
		    width = string.len(data.message) > 20 and consoleWidth,
		    font = my.standardFont,   
		    fontSize = fontSize,
		    align = "left",
		}

		local rect = display.newRect(0,0, text.contentWidth, text.contentHeight)
		text:setFillColor(unpack(Colors:multiply(color, .3)))
		rect:setFillColor(unpack(color))
		rect.alpha = .5
		mGroup:insert(rect)
		mGroup:insert(text)

		mGroup.x = -my.width/2 + rect.contentWidth/2
		mGroup.y = height - rect.contentHeight/2
		mGroup.consoleID = math.random(100000)
		return mGroup
	end

	--severity is a string (one of the four names from 
	--severities above) which determines the color.
	function console:add(message, severity)
		if not CONSOLE then return end --escape!

		--find code
		local prefix = severity
		if not severities[prefix] then prefix = "____" end

		--determine data
		local stayTime = string.len(message) * timeToStayPerChar
		if minStayTime > stayTime then stayTime = minStayTime end
		local stayUntil = system.getTimer() + stayTime
		local color = severities[prefix]

		--prepare for update
		table.insert(messages, {
			message = string.format("%s: %s", prefix , message),
			color = color,
			stayUntil = stayUntil
		})
	end



	function console:clear()
		cleanGroup(messageLayer, true)
	end

	function console:init()
		if not CONSOLE then return end -- excape!

		--draws everything.
		timer.performWithDelay(updateDelay, function()
			console:clear()
			local currHeight = my.height/2
			local currTime = system.getTimer()
			for i = 1, #messages do
				local data = messages[i]
				if data.stayUntil > currTime then
					local obj = createMessageObj(currHeight, data)
					messageLayer:insert(obj)
					currHeight = currHeight - obj.contentHeight
				end
			end
		end, 0) -- go forever!
	end




	console:init()
	return console
end

return Console
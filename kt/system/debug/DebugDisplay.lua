local DebugDisplay = {}

function DebugDisplay:group()
	local g = display.newGroup()
	local center = display.newRect(0,0,10,10)
	center:setFillColor(0,1,0,.5)
	g:insert(center)
	return g
end


return DebugDisplay
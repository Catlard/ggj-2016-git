local DeviceIdentifier = {}

function DeviceIdentifier:new()
	local device = {}

	local iPadModels = {'iPad', 'iPad Simulator'}
	local iPhoneModels = {'iPhone', 'iPod Touch', 'iPod touch', 'iPhone Simulator'}
	local modelName = system.getInfo("model")

	--at first we don't know anything.
    device.isApple = false
    device.isPad = false
    device.isPhone = false

    --is it an apple product?
	if contains(iPadModels, modelName) then 
	    APPSTORE = "apple"
	    device.isApple = true
	    device.isPad = true
	elseif contains(iPhoneModels, modelName) then 
	    APPSTORE = "apple"
	    device.isApple = true
	    if display.pixelHeight >= 1136 then
	        device.isPhoneWide = true
	    else
	        device.isPhone = true
	    end
	end
	
	return device
end


return DeviceIdentifier
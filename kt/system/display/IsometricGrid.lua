--[[this is an isometric grid view object. 
you give it nodes and parameters, and it
builds your grid, which is a table -- a
series of isometrically relevant positions
based on a Screen object, and returns
it to you. it is assumed, in this grid,
that the node at index 0, 0 is at the 
left corner of the isometric display, 
but that the position 0,0 is at the center.

the grid:getPosition function is useful for
moving around the grid, and getting offsets;
for example, to get the offset to move from
0,0 to 3,4, just call grid:getPosition(3,4).]]


local IGrid = {}

function IGrid:new()
	local grid = {}
	 
	local width, height
	local gridHeight, gridWidth
	local nodeWidth, nodeHeight

	--gets initial, untranslated position.
	function grid:getPosition(x, y)
		local xPos = (nodeWidth  * y) + (nodeWidth * x)
		local yPos = (nodeHeight * x) - (nodeHeight * y)
		return {x = xPos, y = yPos}
	end

	function grid:init(params) 
		height = params.height --{nodes, size}
		width = params.width--{nodes, size}
		nodeWidth = width.size
		nodeHeight = height.size
		grid.rows = height.nodes
		grid.columns = width.nodes
		grid.yOff = params.yOff or 0

		grid.positions = {}


		for row = 0, width.nodes-1 do
			for col = 0, height.nodes-1 do
				grid.positions[row+1] = grid.positions[row+1] or {}
				local coords = grid:getPosition(row, col)
				coords.y = coords.y - grid.yOff
				table.insert(grid.positions[row+1], 
							 coords)
			end
		end


	end

	

	function grid:destroy()
		grid.positions = nil
		grid = nil
	end

	return grid
end

return IGrid
local Screen = {}

function Screen:new()

	local screen = display.newGroup()
	screen.x, screen.y = my.centerX, my.centerY

	screen.currentLayer = display.newGroup()
	screen.topLayer = display.newGroup()
	screen.inputLayer = display.newGroup()
	screen.debugLayer = display.newGroup()
	
	screen:insert(screen.currentLayer)
	screen:insert(screen.topLayer)
	screen:insert(screen.inputLayer)
	screen:insert(screen.debugLayer)

	function screen:add(item)
		screen.currentLayer:insert(item)
	end

	return screen

end




return Screen
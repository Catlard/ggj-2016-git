local KittenIO = {}

function KittenIO:saveTable(params)
	local pickled = pickle(params.table)
	local root = params.root or system.ResourceDirectory
	local loc = params.location
	local fileName = params.fileName
	local lfs = require( "lfs" )
	-- Get raw path to app's temporary directory
	local temp_path = system.pathForFile("", root )

	-- Change current working directory
	local success = lfs.chdir( temp_path )  -- Returns true on success
	local new_folder_path

	if success then
	    lfs.mkdir( loc )
	    new_folder_path = lfs.currentdir() .. "/".. loc
	    
	    local relativePath = string.format("%s%s.txt", new_folder_path ,fileName)
    	file = io.open(relativePath, "w")
    	if file then
  			file:write(pickled)
  			file:close()
		    if DEBUG_KITTEN_IO then
		    	print("KITTENIO: Successfully wrote to: " .. relativePath)
		    end
		else
			print("KITTENIO: Could not open file " .. relativePath)
		end
	end
end

function KittenIO:fetchTable(params)
	params.root = params.root or system.ResourceDirectory
	local relativePath = params.location .. params.fileName .. ".txt"

	local absolutePath = system.pathForFile(relativePath, params.root)
	if absolutePath then
		file = io.open(absolutePath, "r")
		if file then
			local data = file:read("*all")
			file:close()
			data = unpickle(data)
			if DEBUG_KITTEN_IO then
				print("KITTENIO: Successfully read from " .. absolutePath)
			end
			return data
		else
			print("KITTENIO: No such file in location ".. absolutePath)
		end
	else
		print("KITTENIO: No such path " .. absolutePath .. ", root " .. params.root)
	end



end




return KittenIO
DataSniper =       require("aStar.scripts.objects.DataSniper")
AStarController =  require("aStar.scripts.AStarController")

local AStarPath = {}

-- GLOBAL OPTIONS FOR EVERY ASTAR OPERATION
DEBUG_ASTAR_DRAWSHAPES =  true -- displays a modifiable set of grid points on the screen, whose visual properties are determined in GridPoint.lua. these points are modifiable when the agent is not moving.
DEBUG_ASTAR_DRAWTEXT =    true -- shows the value for h, g, and f, as well as the node number/status. red gridpoint indexes signify closed points, blue ones
--DEBUG_ASTAR_CONSOLE =     true -- displays the path's open + closed list as it is built. useful for iterative debugging/optimising.
ASTAR_DIAGONAL_MOVEMENT = true -- allows non diagonal movement.

function AStarPath:new(params)

	local aStarPath = {}
	aStarPath.controller = AStarController:new(params) -- create a new controller.
	
	aStarPath.sniper = DataSniper:new("aStarPath")
	aStarPath.sniper:mark(aStarPath)
	aStarPath.sniper:mark(aStarPath.controller.sniper) -- links this sniper to the controller's sniper.


														
	function aStarPath:setGrid(grid) -- set the grid. operations normally cannot be performed unless you do this.
		self.controller:setGrid(grid)
	end

	function aStarPath:showView(isShowing)
		self.controller:showView(isShowing) -- show debug ui (has a text/shape layer. global settings can be fiddled with above)
	end

	function aStarPath:getPath(params) -- expects startVector and finishVector, constructed like Vector:new(3,2)
		return self.controller:getPath(params)
	end

	function aStarPath:calculatePath(params) -- expects a "startVector" and a "finishVector, and iterations, the number of calculations to do. nil will completely calculate the path.
		return self.controller:calculatePath(params)--will return either a LinkedList (the path) or a string state. string states can be "incomplete" if the path is not ready but still being determined, or "impossible" if it has been determined there is no path between the two points.
	end

	function aStarPath:createCustomGrid(params)
		return self.controller:createCustomGrid(params) -- creates a customised grid, assigns it in the model, and returns it to you for later use.
	end

	function aStarPath:destroy()
		aStarPath.sniper:destroy() -- destroys everything in a chain reaction of DataSniper activity.
	end
	
	return aStarPath
end

return AStarPath
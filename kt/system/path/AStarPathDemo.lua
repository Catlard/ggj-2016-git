--==--==--==--==--==--==--==--==--==--==--==
--Welcome to the aStar module.            --
--Simon cares about your sanity,          --
--so he made this demo for you.           --
--Put the following line into             --
--main.lua to run the demo:               --
--										  --
--require("aStar.AStarPathDemo"):runDemo()--
--==--==--==--==--==--==--==--==--==--==--==
--LEAVE THE REST, BELOW, WHERE IT IS. OR  --
--YOU WILL BE THE PERSON WHO BREAKS DEMOS.--
--==--==--==--==--==--==--==--==--==--==--==

local AStarPathDemo = {}

function AStarPathDemo:runDemo()

	--SETUP!
	--first, you'll need to create an aStarPath object instance. the aStarPath will be your primary input-output point for this module.
	AStarPath = require("aStar.AStarPath")
	local aStarPath = AStarPath:new({ isUsingDemoAgent = true, 
									  isUsingDemoGrid  = true }) -- these two commands set up the demo grid objects. remove them to run the module normally.
	--[[normally, you will also need to create a grid, and then 
	use the setGrid function on the AStarPath object to pass it
	in. the controller is handling grid generation automatically for the 
	demo. but here is an explanation of how grid creation works:	]]

	-- params = {} -- First, creating a grid requires a table of parameters. let's make that here.
	-- params.xOffset = 200 -- a manual offset in pixels for the grid. by default centered on the screen.
	-- params.yOffset = 100 -- same, but for y.
	-- params.rowDist = 20 -- a manual option for setting the distance between every row (along the Y axis).
	-- params.colDist = 30 -- the same, but for columns (along the X axis)
	-- params.borderPercent = .1 -- usually 0, don't worry about it. if you choose to have your grid centered in the screen, you can (if you want) ignore the last four parameters, and just tell the grid to be offset from the center by a certain percent of the screen. .25 in this situation will make everything inset such that only the center half of the screen is used by default. the demo uses .1 for this value, so that you can actually see all the nodes on screen, and they're not hidden in the corners. get it?
	-- params.rowsForGrid = 30 -- then number of points along the x axis.
	-- params.colsForGrid = 20 -- the number of points along the y axis. our grid will have 600 points (20 x 30)
	-- params.modVectorList = -- this final part gets a little complex. here we're passing in a list of walls and goals that we want our custom grid to have. it's done by adding GridModification objects to a table. I'll show some different usages of the system.
	-- 	{	
	--         GridModification:new({start=Vector:new(3,5), finish=Vector:new(9,6), type="wall"}), -- this GridModification object assigns a diagonal line of walls. note that a diagonal line drawn this way is drawn with 
	--         GridModification:new({start=Vector:new(10,1), finish=Vector:new(3,4), isHardLine=true, type="wall"}), -- also assigns a line of walls, but without any "diagonal holes" for the path to sneak through.
	--         GridModification:new({start=Vector:new(5,6), type="wall"}), -- this one assigns a single wall. note that no finish vector is needed.
	--         GridModification:new({start=Vector:new(2,2), finish=Vector:new(3,9), isFill=true, type="wall"}), -- this one draws a 2 x 7 block of walls using the isFill parameter. conVEEENient, wouldn't you say?
	--         GridModification:new({start=Vector:new(2,5), finish=Vector:new(3,5), type="empty"}), -- changes a bunch of walls back to empty. 
	--         GridModification:new({start=Vector:new(6,10), type="goal"}), -- this one assigns a single goal.
	-- 	}

	--[[Finally, send it to the aStarPath module, and it will create
	 the grid using a gridFactory, and then return it to you. It will
	 automatically also be assigned in the model, so you don't have to 
	 call aStarPath:setGrid again to work with it. presto magicko! 
	 actually, you can also send these same parameters to the 
	 aStarPath:new() function to create a custom grid on the fly, but 
	 in that case, you won't retain a reference to your custom grid 
	 unless you go in and grab it manually (not an intended functionality).
	]]

	-- local customGrid = aStarPath:createCustomGrid(params)
	-- print("DEMO: Created a custom grid with " .. #customGrid .. " columns and " .. #customGrid[1] .. " rows!") 
	-- --and that's it for grid creation!

	--[[we are creating some vectors to send to the module's grid.
	 the numbers here refer to indexes in the grid's 2d matrix. ]] 
	local startVector=Vector:new(5,1)
	local finishVector=Vector:new(6,10)

	aStarPath:showView(true) -- adds lots of computation time if left on while calculating the path -- at least a second. but can be useful to see why your pathfinding is working a certain way, and if your grid is created correctly.

	-- if you want, you can uncomment this line and cause the demo's goal to be impossible to reach. then you will see the string state "impossible" returned from the calculatePath function.
	aStarPath.controller.model:getGrid()[3][10]:setPointType("wall")






	-- DEMO TEST 1 : this is how you get a complete path in one frame, by making a call to the AStarPath object we created. 
	-- local S = system.getTimer( )
	-- print("DEMO TEST 1: CALCULATING AND RETURNING A PATH.")
	-- local demoPath = aStarPath:getPath({startVector=startVector, finishVector=finishVector})
	-- print("DEMO:Successfully got a path without the view. It was " .. tostring(demoPath:length()) .. " points long. It took ".. ((system.getTimer() - S)/1000) .. " seconds.")







	-- DEMO TEST 2: you can also calculate a path in steps, asynchronously. as long as you pass in the same startVector and finishVector, it will build on the previous calculations. this will allow you to do some load balancing frame to frame.
	print("DEMO TEST 2: CALCULATING A PATH ITERATIVELY.")
	local result = nil

	S  = system.getTimer()
	for i=1, 70 do -- 70 is a good number for the demo map.
		result = aStarPath:calculatePath({iterations=1, startVector=startVector, finishVector=finishVector})
		print("DEMO: After " .. i .. " steps, result was: "..tostring(result))
		
	end
	if type(result) == "table" then
		print("DEMO: Finished calculating path. It was " .. tostring(result:length()) .. " points long. It took ".. ((system.getTimer() - S)/1000) .. " seconds.")
	else
		print("DEMO: Solution found thus far: " .. result)	
	end

	






	--DEMO TEST 3: sets up a box that can be clicked to iteratively travel through and visualize the pathfinding process.
	-- function touch( event )
	--     if event.phase == "began" then
	--         aStarPath.controller:navigateAgent({  --goes to the demo goal using a* pathfinding. additional arguments can be passed.
	--  									iterations = 1,
	--  									isScanIterative = true, -- by default, goes to the demo goal using a* pathfinding. additional arguments can be passed.
	--  									goalVector = finishVector
	--  		}) 
	--     end
	-- end

	-- local iterateButton = display.newRect( 25, 100, 50, 50 )
	-- iterateButton:setFillColor( 1,1,1,1 )
	-- iterateButton:addEventListener( "touch", touch )

	--aStarPath:destroy()
	
	-- to destroy the whole module, just call the destroy function on your aStarPath instance. Everything related to your pathfinding module will be destroyed, in turn.
end

return AStarPathDemo

 
 
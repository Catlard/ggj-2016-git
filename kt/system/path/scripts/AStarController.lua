local AStarController = {}

--CONSTRUCTORS FOR OBJECTS.
AStarModel =       require("aStar.scripts.AStarModel")
AStarView =        require("aStar.scripts.AStarView")

PathFactory =      require("astar.scripts.factories.PathFactory")
GridFactory =      require("aStar.scripts.factories.GridFactory")

Vector =           require("aStar.scripts.objects.Vector")
LinkedList =       require("aStar.scripts.objects.LinkedList")
GridPoint =        require("aStar.scripts.objects.GridPoint")
AStarAgentBase =   require("aStar.scripts.objects.AStarAgent")
GridModification = require("aStar.scripts.objects.GridModification")

CoronaTypeFinder = require("aStar.scripts.utils.CoronaTypeFinder")
GridUtils =        require("aStar.scripts.utils.GridUtils")

--REQUIREMENTS
require("aStar.scripts.utils.DataDumper")
require("aStar.scripts.utils.CleanGroup")

-- if you are just testing the module, you can send nil as the params for aStarMain:new, and the demo will be built. Otherwise you should send your own information.
function AStarController:new(params)


    local aStarController = {}
    aStarController.model = AStarModel:new() -- create a reference to the model.

    aStarController.sniper = DataSniper:new("aStarController")
    aStarController.sniper:mark(aStarController.model.sniper)
    aStarController.sniper:mark(aStarController)


    if not params then params = {} end -- check to make sure that params is really real. really.

    --PARAMETERS for the required grid environment, if you want the controller to build one for you. by default, the demo scene's values. you can also provide one on the fly with the "setgrid" function.------
    params.rowsForGrid = params.rowsForGrid or 10 -- the number of horizontal points (on a grid) for the system to work with.
    params.colsForGrid = params.colsForGrid or 10 -- the number of vertical points (on a grid) for the system to work with.
    params.borderPercent  = params.borderPercent or .1 -- offset from the edge of the screen. 0 is flush with the corners and sides. .5 means everything is squashed in the middle.								
    --PARAMETERS for demo object placement. by default, they are the demo scene's values. ----------------------
    params.startVector = params.startVector or Vector:new(5,1) -- where the path starts.
    params.goalVector = params.goalVector or Vector:new(6,10) -- where the path finishes.
    params.isUsingDemoAgent = params.isUsingDemoAgent or true -- by default, astar will drop in an agent for the demo scene. if you want to turn this off and just get data, don't bother with telling it to drop in the agent.

    params.modVectorList = params.modVectorList or -- demo setup
                                                {   
                                                    GridModification:new({start=Vector:new(6,10), type="goal"}),
                                                    GridModification:new({start=Vector:new(3,5), finish=Vector:new(9,6), isHardLine=true, type="wall"}),
                                                    GridModification:new({start=Vector:new(3,7), finish=Vector:new(6,7), isHardLine=true, type="wall"}),

                                                    GridModification:new({start=Vector:new(5,6), type="wall"}),
                                                    GridModification:new({start=Vector:new(2,2), finish=Vector:new(3,9), isFill=true, type="wall"}),
                                                    GridModification:new({start=Vector:new(2,5), finish=Vector:new(3,5), type="empty"}),
                                                    GridModification:new({start=Vector:new(5,7), finish=Vector:new(4,7), type="wall"}),
                                                    GridModification:new({start=Vector:new(7,7), finish=Vector:new(7,10), type="wall"}),



                                                }
    params.gridProvided = params.gridProvided or nil -- you can provide a grid here, if you like, when setting it up.
    
    if params.isUsingDemoGrid then 
        aStarController.model:newGrid(params)
        params.gridProvided = aStarController.model:getGrid()
    elseif params.gridProvided then 
        aStarController.model:setGrid(params.gridProvided)
    elseif DEBUG_ASTAR_CONSOLE then
        print("Controller didn't receive a grid yet. Will wait for one.")
    end

    if params.isUsingDemoAgent then 
        aStarController.model:setAgent(params.agent or AStarAgentBase:new({isDrawingShapes = DEBUG_ASTAR_DRAWSHAPES, 
                                                       gridPoint = GridUtils:getPoint(params.startVector, aStarController.model:getGrid()) ,
                                                       controller = aStarController,
                                                       model = aStarController.model
                                                      })) -- set up the agent. if you will set up your own, custom agent, then you will need to pass in these four parameters.
    end 

    function aStarController:getPath(params)
        
        return self.model:getFullPath(params.startVector, params.finishVector)
    end

    function aStarController:setGrid(grid)
        self.model:setGrid(grid)
    end

    function aStarController:createCustomGrid(params)
        self.model:newGrid(params)
        return self.model:getGrid()
    end

    function aStarController:showView(newState) -- turns the view on or off.
        if not self.model:getGrid() then 
            print("NO VIEW. You did not provide a grid for this controller, yet.")
            return
        end

        if newState then 
            self.model:showView( {model = self.model, isDrawingShapes ,
                             isDrawingShapes = DEBUG_ASTAR_DRAWSHAPES,
                              isDrawingText = DEBUG_ASTAR_DRAWTEXT })
            self.model:showAgent(true)

        else
            self.model:hideView()
            self.model:showAgent(false)
        end

    end
    
    function aStarController:calculatePath(params)
        return self.model:getPartialPath(params)

    end

    function aStarController:navigateAgent (params)

        if not self.model:getGrid() then 
            print("NO NAVIGATING. You did not provide a grid for this controller, yet.")
            return
        end



        if not params then params = {} end
        --[[OPTIONAL PARAMETERS:
            Vector params.goalVector     ...to pre-define the agent's goal. if not given, the agent will 
                                            search for the goal. provide this as often as possible, if you are
                                            pathfinding asynchronously using the params.isScanIterative parameter.
            number params.iterations     ...to set the number of steps along a path to move.
            bool params.isIgnoringPath   ...to turn off a* pathfinding and move directly to a point.
            bool params.isScanIterative  ...to include the scanning iterations as part of the 
                                            iterations count you give. so, if you set this to true and params.iterations
                                            to 5, then it will step through the scanning process five times, but the agent
                                            might not move, because it might not have found a path to the goal yet -- it
                                            only does 5 rounds of pathfinding.
                                            if, say, finding the destination required 20 scanning iterations and you make
                                            the params.iterations value equal to 21, then it would completely calculate the
                                            path (20 iterations), and move to the first point on the path. this could be useful
                                            for calculating a path asynchronously so that game performance doesn't suffer.
            bool params.isFrozen         ...to prevent the agent from moving after it has finished calculating
                                            the path asynchronously. calling navigate on an agent that has fully calculated its
                                            path but has this parameter will cause the agent to do nothing. why would you want
                                            this? well, let's say you know that you have a lot of pathfinding to do in the future.
                                            perhaps you will calculate 5 iterations per frame until the path is ready,
                                            to get the work done gradually without overloading the cpu.
                                            you don't know how many iterations it will take to calculate a path. but you 
                                            want the agent to wait once the path is fully calculated, and not move until this
                                            function returns true. so you set this parameter until this function (navigateAgent)
                                            returns true. presto! asynchronous pathfinding. we knew you'd love it. this could
                                            also be used as a means of realistically simulating agents' "thinking time".
            

            NOTES: 
            Recalling with the same goal vector argument is more efficient -- 
            the agent will not recalculate the paths if it is not necessary...
            ]]

        return self.model:getAgent():move(params)
    end

    function aStarController:destroy() 
        sniper:destroy()
    end

    return aStarController
end

return AStarController



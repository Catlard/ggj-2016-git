local AStarModel = {}

function AStarModel:new()
	local aStarModel = {}
	
	--things the model can provide
	aStarModel.grid = nil -- where the generated grid is stored.
	aStarModel.view = nil -- all the view objects are contained in a view class.
	aStarModel.agent = nil -- one agent, one model, one controller. 1:1:1

	aStarModel.lastDestinationVector = nil -- how we determine if we are still going to the same place.
	aStarModel.lastPath = nil -- the most recently found path.
	aStarModel.factoryInProgress = nil -- where we store a path factory temporarily, if calculating a path iteratively.

	aStarModel.sniper = DataSniper:new("aStarModel")
	aStarModel.sniper:mark(aStarModel) -- mark it for deletion on destroy.
	


	--VIEW
	function aStarModel:showView(params) 
		if not self.view then
			self.view = AStarView:new(params) 
		else 
			self.view:show()
		end
	end

	function aStarModel:hideView() self.view:hide() end

	
	--GRID
	function aStarModel:getGrid() return self.grid end
	function aStarModel:newGrid(params) 
		if self.factoryInProgress then self.factoryInProgress:destroy() end -- destroy the iterative pathFactory, if one was previously being used.
		self.grid = GridFactory:create(params) end 
	function aStarModel:setGrid(grid) 
		if self.factoryInProgress then self.factoryInProgress:destroy() end -- destroy the iterative pathFactory, if one was previously being used.
		self.grid = grid 
	end
	function aStarModel:cleanGrid(grid) GridUtils:cleanGrid(self.grid) end

	--AGENT
	function aStarModel:showAgent(isShowing) self.agent:show(isShowing) end
	function aStarModel:setAgent(agent) self.agent = agent end
	function aStarModel:getAgent() return self.agent end

	--PATHFINDING
	function aStarModel:getFullPath(startVector, finishVector) -- goes through and gets the whole path if possible.
		self:cleanGrid()
		if self.factoryInProgress then self.factoryInProgress:destroy() end -- destroy the iterative pathFactory, if one was previously being used.
		local S  = system.getTimer()
		self.lastDestinationVector = finishVector
		local pathFactory = PathFactory:new(startVector, finishVector, self:getGrid())
		self.lastPath = pathFactory:tryGetPath()
		if DEBUG_ASTAR_CONSOLE then 
			print ("Getting a path took: " .. ((system.getTimer() - S)/1000) .. " seconds")
		end

		pathFactory:destroy()
		return self.lastPath
	end

	function aStarModel:getPartialPath(params) -- goes through and does a certain number of iterations.
		local isSameDest = false
		if self.lastDestinationVector and 
		   params.finishVector and
		   self.lastDestinationVector.x == params.finishVector.x and
		   self.lastDestinationVector.y == params.finishVector.y then
		   	isSameDest = true
		end


		if not self.factoryInProgress or not isSameDest then -- if this is a new destination, then we need a factory to interact with.
			self.lastDestinationVector = params.finishVector
			if self.factoryInProgress then self.factoryInProgress:destroy() end -- destroy the old one.
			self:cleanGrid()
			if DEBUG_ASTAR_CONSOLE then print ("Created a new path factory for an asychronous operation.") end
			self.factoryInProgress = PathFactory:new(params.startVector, params.finishVector, self:getGrid())
		end
		local result = self.factoryInProgress:tryGetPath(params.iterations)
		self.lastPath = result
		
		return result
	end

	--calls a destroy on every object in the model.
	function aStarModel:destroy() 
		sniper:destroy() 

	end

	return aStarModel
end

return AStarModel



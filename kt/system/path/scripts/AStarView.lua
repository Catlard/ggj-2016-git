local AStarView = {}


function AStarView:new(params) -- immediately, when constructed, a view will go through the grid and draw shapes.
	local aStarView = {}
	
	aStarView.grid = params.model:getGrid()

	function aStarView:init()
		
		for kROW,vROW in pairs(self.grid) do
			for kCOL,vCOL in pairs(vROW) do
				if params.isDrawingShapes then vCOL:drawShapes(point) end
				if params.isDrawingText then vCOL:drawText(point) end
			end
		end
	end

	aStarView:init()
	
	function aStarView:destroy()
		aStarView = nil
	end

	return aStarView
end

return AStarView




GridFactory = {}

function GridFactory:create(params)
	local grid = {}
	local xOffset = params.xOffset or display.contentWidth * params.borderPercent -- for keeping everything visible.
	local yOffset = params.yOffset or display.contentHeight * params.borderPercent
	local rowDist = params.rowDist or (display.contentWidth - (xOffset * 2)) / (params.rowsForGrid - 1) 
	local colDist = params.colDist or (display.contentHeight - (yOffset * 2)) / (params.colsForGrid - 1)
	
	local function modifyGridPoints(params) -- a convenience method for "drawing" solid walls. startXY and endXY are meant to be integer index pairs within the grid -- {1,1} would be the top left. {2,2} would be one unit down and to the right of {1,1}.
		-- print("STARTED NEW MOD")
		-- print(params.start:dump())
		-- print(params.finish:dump())


		--if it's going to be a straight line, then we need to know this stuff.
		local lowX = params.start.x
		local highX = params.finish.x
		local lowY = params.start.y
		local highY = params.finish.y
		
		local newKind = params.type

		-- swap things around so they're in order.
		if lowX > highX then lowX, highX = highX, lowX end
		if lowY > highY then lowY, highY = highY, lowY end
	
		if params.isFill then -- if we're doing a block of changes.
			for i = lowX, highX do
				for j = lowY, highY do
					grid[i][j]:setPointType(newKind)
				end
			end
			return
		end

		if highX-lowX == 0 then -- if the slope will be infinite/undefined, draw a line straight up.
			for yP = lowY, highY do
				grid[lowX][yP]:setPointType(newKind)
			end
			return
		end

		--otherwise, handle it in terms of slope.
		local previousPoint
		local slopeOfMod = (params.finish.y-params.start.y) / (params.finish.x-params.start.x)
		if(math.abs(slopeOfMod) < 1) then -- there need to be extra coordinates added in horizontally.
			
			local yFloat = params.start.y -- a way of counting the y up accurately -- it can be rounded to find the iteration
			for xP = params.start.x, params.finish.x do
				local rounded = math.round(yFloat)
				grid[xP][rounded]:setPointType(newKind)
				yFloat = yFloat + slopeOfMod
				
				previousPoint = rounded
			end
		else -- there need to be extra coordinates added in vertically.
			local xFloat = params.start.x -- a way of counting the x up accurately -- it can be rounded to find the iteration
			for yP = params.start.y, params.finish.y do
				local rounded = math.round(xFloat)
				grid[rounded][yP]:setPointType(newKind)
				xFloat = xFloat + (1/slopeOfMod) -- we have to add the inverse slope!
				previousPoint = rounded
			end
		end
	end

	for row = 1, params.rowsForGrid do 
		grid[row] = {}
		for col = 1, params.colsForGrid do
			gridPoint = GridPoint:new()
			gridPoint:init(xOffset + (rowDist * (row-1)), 
						   yOffset + (colDist * (col-1)),
						   row,
						   col) 
			
			grid[row][col] = gridPoint 
		end
	end
	
	modList = params.modVectorList
	for i = 1, #modList do
		modifyGridPoints(modList[i])
	end 



	return grid

	
end

return GridFactory







local PathFactory = {}

local impossible = "impossible"
local incomplete = "incomplete"

function PathFactory:new(startVector, finishVector, grid)
	local pathFactory = {}
	local sniper = DataSniper:new()
	sniper:mark(pathFactory)

	pathFactory.grid = grid
	pathFactory.closedList = LinkedList:new() --points which are no longer being investigated.
	pathFactory.openList = LinkedList:new() --points which might yield a path. we follow one of these at a time.
	pathFactory.foundPathList = LinkedList:new() -- the path list, which is updated as it is found.

	pathFactory.straightDist = 10 -- cost of going a cardinal direction.
	pathFactory.diagonalDist = 14 -- cost of going diagonally. int'ed to reduce computation time.

	pathFactory.iterationsDone = 0
	pathFactory.currentPathHead = GridUtils:getPoint(startVector,grid)-- the grid point from which the algorithm is currently searching.
	pathFactory.goalPoint = nil-- the gridpoint that the agent is attempting to go to.


	function pathFactory:isPathSolvable()
		if self.openList:length() == 0 and self.closedList:length() > 0 then 
			return false
		else return true end
	end

	function pathFactory:getFullPath(grid) -- keeps going through the path until completion, or until it's stuck.
		
		while not GridUtils:isPathFinished(self.currentPathHead) and self:isPathSolvable() do
			self.iterationsDone = self.iterationsDone + 1
			self:pathFindingStep(grid, self.iterationsDone)
		end

		if not self:isPathSolvable() then return impossible end

		self.foundPathList = LinkedList:new()
		local backtracker = self.currentPathHead
		while backtracker do
			if backtracker then
				self.foundPathList:add(backtracker)
			end
			backtracker = backtracker.parentGridPoint
		end
		if DEBUG_ASTAR_CONSOLE then GridUtils:dumpPath(self.foundPathList) end
		return self.foundPathList
	end	

	function pathFactory:findManhattanDistances(grid, agent_gridPoint, goal_gridPoint)
	  --print(goal_gridPoint.xNdx .. "= x, " .. goal_gridPoint.yNdx .. "= y")
		for kROW,vROW in pairs(grid) do
			for kCOL,vCOL in pairs(vROW) do
				if vCOL.pointType ~= "wall" then
					vCOL:setManhattanDistance(GridUtils:findManhattanDistance(goal_gridPoint, vCOL, self.straightDist))
				end
			end
		end
	end

	function pathFactory:initPath(grid, startPoint, goalPoint) 
		self:findManhattanDistances(grid, startPoint, goalPoint) -- precalculates manhattan distances (g costs), since these do not change.
		self.iterations = 0
		self.goalPoint = goalPoint
		self.currentPathHead = startPoint
	end

	function pathFactory:tryGetPath(iterations)
		if not iterations then
			if DEBUG_ASTAR_CONSOLE then print("Calculated full path.") end
			return self:getFullPath(self.grid) --find the path. bang! so easy. why did this take so long?
		elseif iterations < 1 then 
			if DEBUG_ASTAR_CONSOLE then print("Cannot calculate fewer than 1 iteration.") end
			return nil
		else 
			for i = 1, iterations do
				self:pathFindingStep(grid, i)
				if not self:isPathSolvable() then return impossible end

			end
			if GridUtils:isPathFinished(self.currentPathHead) then 
				if DEBUG_ASTAR_CONSOLE then print("path finished, returned it from factory.") end
				local fin = self:getFullPath(self.grid)
				return fin
			else 
				if DEBUG_ASTAR_CONSOLE then print("path not finished. returned incomplete.") end
				return incomplete
			end
		end
	end

	pathFactory:initPath(grid, GridUtils:getPoint(startVector, grid), GridUtils:getPoint(finishVector, grid)) -- sets up the grid to start.

	function pathFactory:destroy()
		sniper:destroy()
	end

	function pathFactory:addIfValidGridPoint(pointList, pointToTest) -- the method of checking if a point is valid for get surrounding points.
		--print("COORDS: " .. pointToTest.xNdx .. ", " .. pointToTest.yNdx .. ", POINT: " .. (pointToTest:getPointNdxInGrid()) .. "TYPE: " .. pointToTest.pointType)
		if not pointToTest or 
			pointToTest.pointType == "wall" or
			self.closedList:contains(pointToTest) then 
				return pointList -- return it normally if it's not empty.
		else 
			pointList:add(pointToTest) -- add to the list
		end
	end

	function pathFactory:getValidSurroundingPoints(grid, point)
		local surrounding = GridUtils:getSurroundingPoints(grid, point)
		local validList = LinkedList:new()
		local surrTop = surrounding:getTop()
		while surrTop do 
			self:addIfValidGridPoint(validList, surrTop.value)
			surrTop = surrTop.next 
		end
		return validList
	end

	function pathFactory:findBestOpenPoint () -- finds the best point from the open list.
		local l = self.openList:getTop()
		local best
		while l do
			if l.value.pointType == "goal" then -- if we've found the end, then conclude the search.
				best = self.currentPathHead
				l.value.parentGridPoint = self.currentPathHead
				self.currentPathHead = l.value

				return -- cut out early if we find the goal.
			end

			if not best or best:fCost() > l.value:fCost() then
				best = l.value
			end 

			l = l.next
		end
		self.currentPathHead = best
	end

	function pathFactory:scanSurroundingPoints (surroundingPointList)
		local l = surroundingPointList:getTop()
		
		while l do
			local point = l.value
			local dist = GridUtils:getNeighborDistance(self.currentPathHead, point, self.straightDist, self.diagonalDist) -- get the distance between that point and the current head of the path.
	
			if self.openList:contains(point) and point.pathCost > self.currentPathHead.pathCost + dist then
				--print("REROUTE: " .. tostring(point.getPointNdxInGrid()) .. " new parent " .. tostring(currentPathHead) .. "!")
				point.parentGridPoint = self.currentPathHead
			end 

			if not self.openList:contains(point) and not self.closedList:contains(point) then -- if it's not in the open list, add it.
				
				self.openList:add(point)
				if not point.parentGridPoint then -- if it's got no parent, then the parent is the current head.
					point.parentGridPoint = self.currentPathHead
				end
				--print("POINT: " .. point.getPointNdxInGrid() .. " added " .. dist .. ", so total was " .. point.pathCost + dist )
				point.pathCost = self.currentPathHead.pathCost + dist
			end
			
			l = l.next
		end
	end

	function pathFactory:addToClosedList(gridPoint)

		if not gridPoint then 
			return 
		end

		if self.openList:contains(gridPoint) then
		   self.openList:remove(gridPoint)
		end

		if not self.closedList:contains(gridPoint) then
			self.closedList:add(gridPoint)
		else
			if DEBUG_ASTAR_CONSOLE then print("Tried to add a duplicate gridpoint to closed list: " .. gridPoint:getPointNdxInGrid()) end
		end
	end

	function pathFactory:pathFindingStep(grid, iteration)
		if GridUtils:isPathFinished(self.currentPathHead) or not self:isPathSolvable() then return end
		self:addToClosedList(self.currentPathHead) -- make sure the current head is on the closed list.
		local surroundingPointsList = self:getValidSurroundingPoints(grid, self.currentPathHead) -- returns a LIST of points.	
		self:scanSurroundingPoints(surroundingPointsList) -- find the closest objects, and figure out the pathCost values, so we can get an f value for each point. if something has an alternate route that is shorter, that is also determined here.
		self:findBestOpenPoint(surroundingPointsList) -- figure out which of them is the next one to move to. assign it to be the path head.
		GridUtils:updateGridPointText(grid, self.openList, self.closedList)
		if DEBUG_ASTAR_CONSOLE then GridUtils:dumpOpenClosed(self.openList, self.closedList, iteration) end 
	end

	function pathFactory:initPath(grid, startPoint, goalPoint) 
		self:findManhattanDistances(grid, startPoint, goalPoint) -- precalculates manhattan distances (g costs), since these do not change.
		self.iterations = 0
		self.currentPathHead = startPoint
		return self:getFullPath(grid)
	end

	return pathFactory
end

return PathFactory 




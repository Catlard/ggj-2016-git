--[[ this class is intended to be a base class for any aStar agents that you'll
implement. note that the agent is told to move, but does not actually determine
its own path. ]]

local AStarAgent = {}

local agentRadius = 30;
local agentColor = {.5,.8,.7, .5} -- a nice blue, dude. jeez. get off mah back. I should probably make a color util.

function AStarAgent:new(params) 

	local aStarAgent = display.newGroup( )
	local sniper = DataSniper:new()

	sniper:mark(aStarAgent)
	
	aStarAgent.controller = params.controller
	aStarAgent.x = params.gridPoint.xPos
	aStarAgent.y = params.gridPoint.yPos
	aStarAgent.currentGridPoint = params.gridPoint
	aStarAgent.currentPath = nil -- where the path that has most recently been used is stored.
	aStarAgent.prevDestVector = nil -- where the most recent destination is stored. used to determine if we're going to a new place.
	aStarAgent.model = params.model 

	function aStarAgent:destroy()
		sniper:destroy()
	end

	function aStarAgent:show(isShowing) 
		if not isShowing then
			cleanGroup(aStarAgent, true)
			return
		else
			circle = display.newCircle(self.currentGridPoint.xPos, self.currentGridPoint.yPos, agentRadius) 
			--print(gridPoint.xPos .. " , " .. gridPoint.yPos .. " was the position.")
			circle:setFillColor(unpack(agentColor)) -- i assume that if I only had a group, I would need to walk through the objects in the group to set their fill colors, since I can set the fill color for a display object, but not a group, right?
			aStarAgent:insert(circle, true)
		end
	end

	function aStarAgent:hide()
		
	end

	function aStarAgent:goToPoint(newDestination) -- this function does not pathfind. it just moves in a direct line to the point specified.
		self.currentGridPoint = newDestination
		transition.moveTo(self, {x=newDestination.xPos, 
										   y=newDestination.yPos, 
										   time=1000, 
										  })
	end

	function aStarAgent:getGridPoint()
		return self.currentGridPoint
	end

	function aStarAgent:checkForValidGoal(params)
		if not params.goalVector then 
			local goalPoint = GridUtils:findGoal(self.model:getGrid())
			params.goalVector = Vector:new(goalPoint.xNdx, goalPoint.yNdx)
		end
		return params
	end

	function aStarAgent:setUpReturnTable(newBool, goalVector)
		local returned = {}
		returned.isFinishedScanning = newBool
		returned.isTraversable = newBool
		if goalVector then 
			returned.goalVector = goalVector
		end

		return returned
	end

	

	function aStarAgent:move(params) -- this is the function that the controller calls. the agent will contact the model and ask for a path to the goal point, or just move directly, as per the parameters.
		--TO DO: need to stop other navigation attempts here. should be able to interrupt scanning and pathfinding events.                   
		params = self:checkForValidGoal(params) -- checks the goalpoint, and finds it if necessary.
		returned = self:setUpReturnTable(false, params.goalVector)

		if not params.goalVector then return returned end -- if there's no goal point, then we're out.

		if params.isIgnoringPath then -- if we don't care about pathfinding.
			self:goToPoint(self.model:getPoint(params.goalVector)) -- navigate to the goal directly. screw pathfinding!
		elseif self.currentPath and not params.isFrozen and self:isSameDestination(params.goalVector) then -- if we're going to the same place and the path has been calculated, no reason to recalc the same path.	
			self:followPath(self.path, params.iterations) -- move it, baby, yeah!
		else 
			local currentVector = Vector:new(self.currentGridPoint.xNdx, self.currentGridPoint.yNdx)
			local path = nil
			if params.isScanIterative then
				path, params.iterations = self.model:getPartialPath({startVector=currentVector, 
																	 finishVector=params.goalVector, 
																	 iterations=params.iterations})-- calculate part of the path, retain the iterations remaining.
			else
				path = self.model:getFullPath(currentVector, params.goalVector) -- calculate the whole path, don't change the iterations.
			end

			if path and type(path) == "table" and not params.isFrozen then
				self.currentPath = path
				self:followPath(path, params.iterations) -- follows the path given by the controller. goes a specified distance specified by the number in params.iterations. for example, making this number 5 means the agent will move 5 points along the path.
			end
		end

		prevDestVector = params.goal
		return returned
	end

	function aStarAgent:isSameDestination(newDestVector)
		if prevDestVector and prevDestVector:dump() == newDestVector:dump() then 
			return true
		else 
			return false 
		end

	end

	function aStarAgent:followPath(path, gridPointsToGo) -- moves a certain number of steps, or finishes the path if not given a number of steps to go (or if you try to travel too many steps...)		

		local pathPoint = path:getTop()
		if not pathPoint or -- if the path is invalid, or it has ended.
			   (gridPointsToGo and gridPointsToGo < 0) then -- or if we shouldn't move anymore
			return --then stop
		end -- fugghetaboutit

		local function onComplete ()
			self:agentFinishedMoving(path, gridPointsToGo) 
		end

		self.currentGridPoint = pathPoint.value
		transition.moveTo(self, {x=pathPoint.value.xPos, 
								 y=pathPoint.value.yPos, 
								 time=1000, 
								 onComplete = onComplete
								})
	end

	function aStarAgent:agentFinishedMoving(path, gridPointsToGo) -- loops back to call follow path again.
		local remaining = nil
		if gridPointsToGo then remaining = gridPointsToGo -1 end
		path:pop() -- pops the latest point off the top of the list. doesn't store it anywhere.
		self:followPath(path, remaining)
	end

	return aStarAgent

end

return AStarAgent

local DataSniper = {}

--[[this class is meant to help destroy objects in sequences and hierarchies automatically. 
basically, in an OOP class, you declare a sniper and tell it to mark the object it is in 
(a table). then, you have it mark all the child objects with snipers. when you call the 
destroy function on the parent sniper, the child objects will also activate and destroy 
their tables, and so on, until the all the marked targets are destroyed.]]


function DataSniper:new(sniperName)
	local dataSniper = {}
	 dataSniper.isADataSniper = true -- when other datasnipers need to activate this sniper's destroy function.
	 dataSniper.items = LinkedList:new()
	 dataSniper.itemCount = 0
	 dataSniper.name = nil
	if sniperName then dataSniper.name = sniperName end

	function dataSniper:mark(item) -- marks an item for nil'ing later. when declaring a variable, you should mark it with this function. then, in the destroy function of your classes, you should call the datasniper's destroy function, and everything will be happy.
		self.itemCount = self.itemCount + 1
		self.items:add(item)
		-- if(self.name) then 
		-- 	print(tostring(self.name) .. " marked " .. tostring(item))
		-- end
	end

	function dataSniper:destroy() -- recursively destroys everything in the table items.
		local nameStr = "Sniped "
		if self.name then nameStr = self.name .. " sniped " end
		if not self.items then return end
		print(nameStr .. self.items:length() .. " items." )

		self:destroyList(self.items) -- this call starts a recursive chain.
		self.items = nil
		self.itemCount = nil
		self.isASniper = nil
		self.itemCount = nil
		self.name = nil
		self = nil
	end

	--this function NEEDS to destroy tables or lists recursively, but id'ing lists or tables hasn't been done yet. i've started the other one below.
	function dataSniper:destroyList(list)
		local t = list:getTop()
		while t do
			self:nilItem(t.value)
			t = t.next
		end
		t = nil;
	end

	function dataSniper:nilItem(item)



		--print("Attempted to delete: " .. tostring(item) .. " a " .. type(item))
		
		if item and -- it's not nil
		   type(item) == "table" and -- it's a table
		   item ~= dataSniper then -- it's not this object
			    local coronaType = nil
				coronaType = CoronaTypeFinder:findType(item) 
				if item.isADataSniper then -- if it's a sniper, call that sniper's destroy function.
					item:destroy()
				elseif coronaType == "displayGroup" then
					cleanGroup(item, true)
				else -- if it's a table, call the destroying table function.
					self:destroyTable(item) 
				end
		end
		item = nil
	end

	function dataSniper:destroyTable(table)
		for key, value in pairs(table) do
			self:nilItem(value)
			--if type(value) == "table" then
		end
	end

	return dataSniper
end

return DataSniper



-- a class that handles an ordered pair of coordinates.

local GridModification = {}

function GridModification:new(params)

	local gridModification = {}
	gridModification.start = params.start 
	gridModification.finish = params.finish or gridModification.start
	gridModification.type = params.type
	gridModification.isFill = params.isFill or nil
	return gridModification
end

return GridModification


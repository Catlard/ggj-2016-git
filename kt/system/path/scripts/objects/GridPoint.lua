--[[this is the base object for all grid locations. this object, when
created and placed in a grid by the grid factory, is used to calculate 
aStar paths. --]]

local GridPoint = {}

local fontSize = 14

local emptyRadius = 20 -- the display size for the empty, traversable grid points.
local emptyColor = {1,0,0,.3}

local wallRadius = 50 -- display size for non-traversable grid points.
local wallColor = {1,0,0,.8}

local goalRadius = 40 -- display size for a goal point.
local goalColor = {1,1,1,.3}

function GridPoint:new()
	
	local gridPoint = {}
	local sniper = DataSniper:new()
	sniper:mark(gridPoint)

	function gridPoint:init(x,y,xNdx,yNdx) 
		self.xPos = x
		self.yPos = y -- positioning vars
		self.xNdx = xNdx -- these are the index positions within the gridpoint table.
		self.yNdx = yNdx
		self.pointType = "empty" --[[ can be: empty (empty space), 
											  	  wall (impassible), 
											 	  goal (desired position)]]
        self:resetPathVariables()


	end

	function gridPoint:resetPathVariables()
		self.status = "none" -- for use with open and closed lists. can be none, open, or closed.
		self.manhattanDistance = 0
        self.pathCost = 0
        self.parentGridPoint = nil -- this is how we store the chain of open points. 
        self:setPointType(self.pointType)
	end

	function gridPoint:fCost() return self.manhattanDistance + self.pathCost end

    function gridPoint:setManhattanDistance(value) self.manhattanDistance = value end

    function gridPoint:setPathCost(value) self.pathCost = value end

	function gridPoint:destroy() sniper:destroy() end -- boom

	function gridPoint:getPointNdxInGrid () return (gridPoint.yNdx * 10) + gridPoint.xNdx - 10 end

	function gridPoint:setPointType(newKind) 
		self.pointType = newKind
		if gridPoint.shapeGroup then self:drawShapes() end
		if gridPoint.textGroup then self:drawText() end

	end

	function gridPoint:drawText() -- h is the manhattan dist, g is the path distance thus far

		if self.textGroup then
			cleanGroup(self.textGroup, true)
		else 
			self.textGroup = display.newGroup()
		end		

		local textObj

		local options = {
		    parent = self.textGroup,
		    x = self.xPos,
		    y = self.yPos,
		    width = 128,     --required for multi-line and alignment
		    font = native.systemFont,   
		    fontSize = fontSize,
		    align = "center"  --new alignment parameter
		}

		if self.manhattanDistance > 0 then
			options.text = "h=".. self.manhattanDistance 
		    options.y = gridPoint.yPos + 20
			textObj = display.newText(options)
			textObj:setFillColor( 1,1,0,1 )
			self.textGroup:insert(textObj)
		end
		
		if self.pathCost > 0 then
			options.text = "g=" .. self.pathCost
			options.y = options.y - 15
			textObj = display.newText(options)
			textObj:setFillColor( 0,1,0,1 )
			self.textGroup:insert(textObj)
		end

		if self.manhattanDistance > 0 and self.pathCost > 0 then
			options.text = "f=".. (self:fCost())
			options.y = options.y - 15
			textObj = display.newText(options)
			textObj:setFillColor( 0,1,1,1 )
			self.textGroup:insert(textObj)

		end

		-- finally, set the index point text

		options.text = self:getPointNdxInGrid()
		options.y = options.y - 15
		textObj = display.newText(options )
		if self.status == "none" then
			textObj:setFillColor( 1,1,1,1 )
		elseif self.status == "closed" then
			textObj:setFillColor( 1,0,0,1 )
		elseif self.status == "open" then
			textObj:setFillColor( 0,0,1,1 )
		end
		gridPoint.textGroup:insert(textObj)

	end	

	function gridPoint:drawShapes()


		if self.shapeGroup then
			cleanGroup(self.shapeGroup, true)
		else 
			self.shapeGroup = display.newGroup( )
		end

		local circle 

		if self.pointType == "wall" then
			circle = display.newRect(self.xPos, self.yPos, wallRadius, wallRadius)
			circle:setFillColor(unpack(wallColor)) 
		elseif self.pointType == "goal" then
			circle = display.newCircle(self.xPos, self.yPos, goalRadius)
			circle:setFillColor(unpack(goalColor)) 
		elseif self.pointType == "empty" then
			circle = display.newCircle(self.xPos, self.yPos, emptyRadius)
			circle:setFillColor(unpack(emptyColor))
		end
		self.shapeGroup:insert(circle)
	end

	return gridPoint
end

return GridPoint

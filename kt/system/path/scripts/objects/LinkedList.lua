local LinkedList = {}

function LinkedList:new()

	local linkedList = {}
	local top = nil

	function linkedList:getTop() -- returns the head node, for iteration.
		return top
	end

	function linkedList:append(listToAppend) -- adds all the values from a given list into this list. takes a list as an argument. 
		appendTop = listToAppend:getTop()
		while appendTop do
			self:add(appendTop.value)
			appendTop = appendTop.next
		end

	end

	function linkedList:getBottom() -- gets the most buried node, returns a reference to it.
		local bottom = top -- so we can start crawling.
		while bottom.next do
			bottom = bottom.next
		end
		return bottom
	end

	function linkedList:pop()
		local popped = top
		top = top.next
		return popped
	end

	function linkedList:reverse() -- flips the list, so now the most recent head comes last. all those people on the internet who said that this couldn't be done with a singly linked list are FRIGGIN IDIOTAS.
		reversed = LinkedList:new()
		while top do
			reversed:add(top.value)
			top = top.next
		end

		top = reversed:getTop()

	end

	function linkedList:length() -- returns a number length for list.
		local l = top
		local count = 0
		while l do
			count = count + 1
			l = l.next
		end

		return count
	end

	function linkedList:add(item) top = {next = top, value = item} --adds item to the list.
	end

	function linkedList:dump()
		if not top then 
			print("Can't dump. Linked list: " .. tostring(top) .. " was empty.")
		end


		local t = top

		while t do
			print("ITEM IN LIST: " .. tostring(t.value))
			t = t.next
		end
	end


	function linkedList:contains(item) -- returns true if list contains item, false if it doesn't.
		local t = top
		while t do
			if(t.value == item) then 
				return true 
			end
			t = t.next
		end
		return false
	end

	function linkedList:remove(item)
		if not self:contains(item) then 
			print("Linked List: " .. tostring(self) .. " did not contain " .. type(item) .. ": " .. item .. ". Nothing removed.")
			return
		end

		local t = top
		local prev = t
		while t do
			if t.value == item then
				if prev == t then -- if we're trying to remove the top element
					top = top.next
					return
				else 
					prev.next = t.next
					return
					
				end
				print("Removed: " .. t.value)
				return
			end
			prev = t
			t = t.next
		end
	end

	return linkedList
end

return LinkedList


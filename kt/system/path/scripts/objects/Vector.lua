-- a class that handles an ordered pair of coordinates.

local Vector = {}

function Vector:new(x, y)

	local vector = {}
	vector.x = x
	vector.y = y

	function vector:dump()
		return "(" .. vector.x .. "," .. vector.y .. ")"
	end 
	
	return vector
end

return Vector


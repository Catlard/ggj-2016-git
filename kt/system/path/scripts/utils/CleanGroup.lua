function cleanGroup( objectOrGroup, preserveSelf )

    if not objectOrGroup then return end

    if preserveSelf then 
        if objectOrGroup.numChildren then
            while objectOrGroup.numChildren > 0 do
                local child = objectOrGroup[objectOrGroup.numChildren]
                display.remove(child)
                child=nil
            end
        end
    else
        display.remove(objectOrGroup)
        objectOrGroup=nil
    end

end



local CoronaTypeFinder = {}


function CoronaTypeFinder:findType(object)
	if object and object.numChildren then return "displayGroup"

	else return "typeNotFound" end
end

return CoronaTypeFinder

local GridUtils = {} -- the gridUtils know what a gridPoint is, but know nothing about the specific settings of the environment.


function GridUtils:isDiagonal(pA, pB) -- returns true if points have diagonal relationships. else false.
	local aX = pA.xNdx
	local aY = pA.yNdx
	local bX = pB.xNdx
	local bY = pB.yNdx

	if math.abs(aX - bX) + math.abs(aY - bY) ~= 1 then 
		return true
	else 
		return false 
	end
end

function GridUtils:getPoint(vector, grid)
	if not grid then 
		print("Tried to find a point on a nil grid.") 
		return nil 
	else 
		return grid[vector.x][vector.y]
	end
end

function GridUtils:cleanGrid(grid)
	for kROW,vROW in pairs(grid) do
		for kCOL,vCOL in pairs(vROW) do
			vCOL:resetPathVariables()
		end
	end
end

function GridUtils:findGoal(grid) -- brute force searches for ONE goal. very non-optimal.
	for kROW,vROW in pairs(grid) do
		for kCOL,vCOL in pairs(vROW) do
			if vCOL.pointType == "goal" then 
				return vCOL 
			end
		end
	end
	print("could not find a goal.")
	return nil
end

function GridUtils:findManhattanDistance(goal_gridPoint, current_gridPoint, straightDist) 
	return (math.abs(goal_gridPoint.xNdx - current_gridPoint.xNdx) + 
		    math.abs(goal_gridPoint.yNdx - current_gridPoint.yNdx)) * straightDist
end

function GridUtils:getNeighborDistance(pA, pB, straightDist, diagDist) -- uses the isDiagonal function to apply path costs in the grid.
	local dist = diagDist
	if not GridUtils:isDiagonal(pA, pB) then
			dist = straightDist
	end

	return dist
end

function GridUtils:getSurroundingPoints(grid, pointToFindSurroundingElementsFor) -- basically, scans the points surrounding a point, and adds them to a list if they're valid/traversible/open.
	xNdx = pointToFindSurroundingElementsFor.xNdx
	yNdx = pointToFindSurroundingElementsFor.yNdx
	cols = #grid
	rows = #grid[1]
	local surroundingPoints = LinkedList:new()

	--top row.
	if yNdx < rows then
		if xNdx > 1    and ASTAR_DIAGONAL_MOVEMENT then surroundingPoints:add(grid[xNdx-1][yNdx + 1]) end
						    							surroundingPoints:add(grid[xNdx-0][yNdx + 1])
		if xNdx < cols and ASTAR_DIAGONAL_MOVEMENT then surroundingPoints:add(grid[xNdx+1][yNdx + 1]) end
	end

	--middle row.
	if xNdx > 1    then surroundingPoints:add(grid[xNdx-1][yNdx + 0]) end
	if xNdx < cols then surroundingPoints:add(grid[xNdx+1][yNdx + 0]) end

	--bottom row.
	if yNdx > 1 then
		if xNdx > 1    and ASTAR_DIAGONAL_MOVEMENT then surroundingPoints:add(grid[xNdx-1][yNdx - 1]) end
							surroundingPoints:add(grid[xNdx-0][yNdx - 1])
		if xNdx < cols and ASTAR_DIAGONAL_MOVEMENT then surroundingPoints:add(grid[xNdx+1][yNdx - 1]) end
	end

	return surroundingPoints
end

function GridUtils:dumpPath(path)
	local pathString = "PATH WAS: "
	local pathTop = path:getTop()
	while pathTop do
		pathString = pathString .. tostring(pathTop.value:getPointNdxInGrid()) .. ", "
		pathTop = pathTop.next
	end
	print("----------")
	print(pathString)
	print(path:length() .. " was length of found path.")
	print("----------")
end

function GridUtils:dumpOpenClosed(openList, closedList, iteration)
	
	local numStr = ""

	if openList then
		oL = openList:getTop()
		print(iteration .. ": THIS GEN OPEN----------------: ")
		while oL do
			numStr = numStr .. oL.value:getPointNdxInGrid() .. ", "
			oL = oL.next
		end
		print(numStr)
		numStr = ""
	end
	
	if closedList then
		cL = closedList:getTop()
		print(iteration .. ": THIS GEN CLOSED----------------: ")
		while cL do
			numStr = numStr .. cL.value:getPointNdxInGrid() .. ", "
			cL = cL.next
		end
		print(numStr)
	end
end

function GridUtils:isPathFinished(currentPathHead)
	if currentPathHead and currentPathHead.manhattanDistance == 0 then 
		return true -- the path has been completely built.
	else 
		return false -- the path is partially built.
	end
end

function GridUtils:updateGridPointText(grid, openList, closedList) -- update the debug gui.
	--dumpOpenClosed()
	--print(goal_gridPoint.xNdx .. "= x, " .. goal_gridPoint.yNdx .. "= y")
	for kROW,vROW in pairs(grid) do
		for kCOL,vCOL in pairs(vROW) do
			if vCOL.pointType ~= "wall" then
				if openList:contains(vCOL) then
					vCOL.status = "open"
				elseif closedList:contains(vCOL) then
					vCOL.status = "closed"
				else
					vCOL.status = "none"
				end
				vCOL:setPointType(vCOL.pointType)
			end
		end
	end
end

return GridUtils
--the debug button.
    local function touched (event)
        if event.phase == "began" then
            aStarMain:buildPath(1)
        end
    end

    function drawBackdrop(w, h, xOffset, yOffset) 

	local myRectangle = display.newRect( w/2 + xOffset, h/2 + yOffset, w, h )
	myRectangle.strokeWidth = 3
	myRectangle:setFillColor( 0.5 )
	myRectangle:setStrokeColor( 1, 0, 0, .3)
end


drawBackdrop(display.contentWidth - xOffset * 2, display.contentHeight - yOffset * 2, xOffset, yOffset) -- a background comprising the whole astar area for debug testing...
	
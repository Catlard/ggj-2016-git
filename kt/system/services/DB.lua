local DB = {}

function DB:new()

    require "sqlite3"
    local crypto = require "crypto"

    local algorithm = crypto.sha1
    --local algorithm = crypto.md4

    --Open data.db.  If the file doesn't exist it will be created
    --
    local path = system.pathForFile("data.db", system.DocumentsDirectory)
    local db = sqlite3.open(path)   

    --print ("DB: " .. path)
    --Handle the applicationExit event to close the db
    local function onSystemEvent( event )
        if( event.type == "applicationExit" ) then              
            db:close()
        end
    end

    Runtime:addEventListener( "system", onSystemEvent )
    
    local function setOption(key, value)
        local query =[[DELETE FROM options WHERE key = ']] .. key .. [[';]]
        --print (query)
        db:exec(query)
        local query =[[INSERT INTO options VALUES (null, ']] .. key .. [[',']] .. value .. [['); ]]
        --print (query)
        db:exec(query)
    end

    local function getOption(key, default)
        local query = [[SELECT value FROM options WHERE key=']] .. key .. [[']]
        --print (query)

        local value = default

        for row in db:nrows(query) do 
            value = row["value"]
        end

        return value
    end

    local function setLiked( liked )
        if liked then
            setOption( 'liked', 'y')
        else 
            setOption( 'liked', 'n')
        end
    end

    local function isLiked()
        local l = getOption( 'liked', 'n')
        if l == 'y' then
            return true
        end
        return false
    end

    -- clear all purchases
    if TESTING_PURCHASES then
        local query = [[DROP TABLE purchases;]]
        db:exec(query)
        setOption("visited_lesson_menu", "false")
    end

    --setup tables if they don't exist
    local query = [[CREATE TABLE IF NOT EXISTS hiscores (id INTEGER PRIMARY KEY, game, name, score);]]
    db:exec(query)

    local query = [[CREATE TABLE IF NOT EXISTS options (id INTEGER PRIMARY KEY, key, value);]]
    db:exec(query)

    local query = [[CREATE TABLE IF NOT EXISTS purchases (id INTEGER PRIMARY KEY, lesson);]]
    db:exec(query)

    local query = [[CREATE TABLE IF NOT EXISTS news (id INTEGER PRIMARY KEY, date);]]
    db:exec(query)

    --print the sqlite version to the terminal
    --print( "version " .. sqlite3.version() )
    --


     
    -- returns the top [count] entries from [game]
    local function top(game, count, desc)

        --print all the table contents
        local query = [[SELECT name, score FROM hiscores WHERE game=']] .. game .. [[' ORDER BY score ]]
        if desc then query = query .. [[DESC]] end
        query = query .. [[ LIMIT ]] .. count

        --print (query)

        local rows = {}

        for row in db:nrows(query) do 
            table.insert(rows, row)
        end

        return rows

    end

    local function submit(game, name, score)
        local query =[[INSERT INTO hiscores VALUES (NULL, ']] .. game .. [[',']] .. name .. [[',]] .. score ..[[); ]]
        --print (query)
        db:exec(query)
    end

    local function rank(game, score, scoreType)
        local operator
        if scoreType == "time" then
            operator = "<="
        else
            operator = ">="
        end

        local query = [[SELECT count(*) FROM hiscores WHERE game = ']] .. game .. [[' AND score ]] .. operator .. [[ ]] .. score
        --print (query)

        local r = 0

        for row in db:nrows(query) do 
            r = row["count(*)"]
        end

        return r
    end

    local function resetScores()
        local query =[[DELETE FROM hiscores;]]
        --print (query)
        db:exec(query)
    end

    --submit('chess', 'aaa', 5)
    --submit('chess', 'baa', 6)
    --submit('chess', 'haa', 11)
    --submit('chess', 'aja', 12)

    --local rows = top('chess', 5, true)

    --for _, row in ipairs(rows) do 
        --print (row.name .. " " .. row.score)
    --end
    --

    local function incrementMenuVisitCount()
        local visits = getOption("menuVisits", 0) -- default value
        visits = visits + 1
        setOption("menuVisits", visits)
        return visits
    end

    -----------------------------------------
    -- in-app purchases
    --
    --

    local function insertPurchase(id)
        print ("db:insertPurchase purchasing single lesson : " .. id)

        -- don't record the same purchase twice: delete it before inserting it
        local hash = crypto.digest(algorithm, id)

        local query =[[DELETE FROM purchases WHERE lesson = ']] .. hash ..[['; ]]
        --print (query)
        db:exec(query)
        local query =[[INSERT INTO purchases VALUES (NULL, ']] .. hash ..[['); ]]
        --print (query)
        db:exec(query)
    end

    local function getPurchases()

        --print all the table contents
        local query = [[SELECT lesson FROM purchases]]
        --print (query)

        local purchases = {}
        for row in db:nrows(query) do 
            table.insert(purchases, row.lesson)
        end
        return purchases
    end
    
    -- return number of lessons purchased
    local function getPurchaseCount()
        local purchases = getPurchases()
        return #purchases
    end

    local function getRemainingPurchaseCount()
        local count = getPurchaseCount()
        local singleProducts = purchases.products.singleProductIDList
        local total = #singleProducts
        return total-count
    end

    local function productIsPurchased(id)
        local hash = crypto.digest(algorithm, id)
        return table.contains(getPurchases(), hash) or ALL_LESSONS_OPEN
    end

    local function deleteAllPurchases()
        local query =[[DELETE FROM purchases;]]
        --print (query)
        db:exec(query)
    end

    local function deletePurchaseByHash( hash )
        local query =[[DELETE FROM purchases WHERE lesson = ']] .. hash ..[['; ]]
        db:exec(query)
    end

    local function getMustRestoreFlag() 
        local value = getOption("must_restore", "true")
        return value == "true"
    end 

    function setMustRestoreFlag()
        setOption("must_restore", "false")
    end




    ----------------------
    -- NEWSFLASH

    -- search by date
    local function newsflashHasBeenViewed(date)
        local query = [[SELECT date FROM news WHERE date = ']] .. date .. [[']]
        local count=0
        for row in db:nrows(query) do 
            count=count+1
        end
        --print ("db.newsflashHasBeenViewed found " .. count .. " matching news items for date=" .. date)
        return count>0
    end

    local function viewNewsflash(date)
        local query =[[INSERT INTO news VALUES (null, ']] .. date .. [['); ]]
        --print (query)
        db:exec(query)
    end


 
    return {
        getOption=getOption,
        setOption=setOption,
        setLiked=setLiked,
        isLiked=isLiked,
        top=top,
        submit=submit,
        resetScores=resetScores,
        rank=rank,
        incrementMenuVisitCount=incrementMenuVisitCount,

        insertPurchase=insertPurchase,
        productIsPurchased=productIsPurchased,
        getPurchases=getPurchases,
        getPurchaseCount=getPurchaseCount,
        getRemainingPurchaseCount=getRemainingPurchaseCount,
        deleteAllPurchases=deleteAllPurchases,
        deletePurchaseByHash=deletePurchaseByHash,

        setMustRestoreFlag=setMustRestoreFlag,
        getMustRestoreFlag=getMustRestoreFlag,

        newsflashHasBeenViewed=newsflashHasBeenViewed,
        viewNewsflash=viewNewsflash
    }
end

return DB

     


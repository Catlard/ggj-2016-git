local DocGetter = {}

function DocGetter:new()

    local getter = {}
    local json = require("json")
    local sNdx = string.len("cokwr:  ") --something like this is before every piece of data

    local function getCleanRow(dirtyRow)
        local content = dirtyRow.content["$t"]
        content = string.sub(content, 2) --remove a leading _
        local cleanRow = string.split(content, ", _" )
        for i = 1, #cleanRow do
            cleanRow[i] = string.sub(cleanRow[i], sNdx)
            --remove slashes
        end 
        return cleanRow
    end

    local function cleanSheet(params)
        local clean = {}
        local dirty = params.data
        local format = params.format or "rowsAreObjects"
        local topRow = getCleanRow(dirty[1], 1)
        for i = 2, #dirty do
            cleanRow = getCleanRow(dirty[i])
            if format == "rowsAreObjects" then
                local kvRow = {}
                for j = 1, #cleanRow do
                    kvRow[topRow[j]] = cleanRow[j]
                end
                cleanRow = kvRow
            end
            table.insert(clean, cleanRow)
        end
        return clean
    end

    function getter:fetch(params)
        local fetched = {}
        local sheets = 0
        for sheet, def in pairs(params.sheets) do sheets = sheets + 1 end
        for sheet, def in pairs(params.sheets) do
            local function listener(event)
                event.sheetTitle=def.title
                event.sheetFormat=def.format
                local decoded = json.decode(event.response)
                local cleanedData = cleanSheet{data = decoded.feed.entry, format = def.format}
                fetched[def.title] = cleanedData
                sheets = sheets - 1
                if DEBUG_GOOGLE_FETCHES then
                    print("GOOGLE DOC FETCHER: Got "..def.title)
                end
                if sheets == 0 then
                    params.callback(fetched)
                end
            end
            local URI = string.format("https://spreadsheets.google.com/feeds/list/%s/%s/public/basic?alt=json", 
                                    params.key, sheet)
            network.request( URI, "GET", listener)
        end
    end 

    return getter
end



return DocGetter
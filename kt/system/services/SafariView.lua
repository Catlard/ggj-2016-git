local SafariView = {}
local widget = require( "widget" )

function SafariView:new(params)
	local view = {}

	local function destroy()
		view = nil
	end

	-- 2) Create a listener function for Safari View events
	local function safariListener( event )

	    if ( event.action == "failed" ) then
	        if params.onError then params.onError() end
	        print( "Error loading the URL" )
	    elseif ( event.action == "loaded" ) then
	        if params.onLoaded then params.onLoaded() end
	        print( "Page loaded!" )
	    elseif ( event.action == "done" ) then
	      	if params.onClosed then params.onClosed() end
	        print( "Safari view closed" )
	        destroy()
	    end
	end

	-- 3) Set up a function to show the Safari View
	local function showWebsite( event )
	    -- 4) Check if the Safari View is available
	    local safariViewAvailable = native.canShowPopup( "safariView" )
	  
	    -- 5) If available, create options table and show the Safari View
	    if safariViewAvailable then
	        local popupOptions = {
	            url = params.website,
	            animated = true,
	            entersReaderIfAvailable = true,
	            listener = safariListener
	        }
	        native.showPopup( "safariView", popupOptions )
	    else
	    	print("SAFARI VIEW: Cannot show native popup. Probably because you're in the simulator?")
		end
	end

	showWebsite()

	return view
end

return SafariView
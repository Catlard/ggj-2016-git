local Sheets = {}

function Sheets:new()
	local sheets = {}
	local activeSheets = {}

	function sheets:add(name, fileName) -- the name of the file in the sheetPath.
		if activeSheets[name] then 
			print("WARNING: A sheet with name", name, "already existed. Deleted the first sheet. Overwrote it.")
		end

		activeSheets[name] = SpriteSheet:new(fileName)
	end

	function sheets:getSprite(sheetName, spriteName)
		if not activeSheets[sheetName] then
			print("ERROR: Attempt to get sprite", spriteName, "from sheet", sheetName, "failed because sheet was not valid or loaded or something.")
		end
		return activeSheets[sheetName]:getSprite(spriteName)
	end

	function sheets:getAnimatedSprite(sheetName, sequenceData)
		if not activeSheets[sheetName] then
			print("ERROR: Attempt to get ANIMATED sprite", spriteName, "from sheet", sheetName, "failed because sheet was not valid or loaded or something.")
		end
		return activeSheets[sheetName]:getAnimatedSprite(sequenceData)
	end

	function sheets:remove(name)
		print("SHEET REMOVE NOT IMPLEMENTED")
	end

	return sheets
end

return Sheets
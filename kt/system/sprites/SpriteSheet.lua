local SpriteSheet = {}

function SpriteSheet:new(infoPath)
    local sheet = {}
    local imagePath 
    local sheetInfo 
    local imageSheet 

    function sheet:init()
        imagePath = string.gsub(infoPath, "%.", "/")..".png"
        sheetInfo = require(infoPath)
        imageSheet = graphics.newImageSheet( imagePath, sheetInfo:getSheet() )
        sheet.sheetInfo, sheet.imageSheet = sheetInfo, imageSheet
    end


    function sheet:getAnimatedSprite(sequenceData)
        -- sequenceData.start = sheetInfo:getFrameIndex(sheet.startFrameName) -- first frame
        -- dump(sequenceData)

        return display.newSprite(imageSheet, sequenceData)
        
    end

    function sheet:getSprite(id, params)
        if not id then print("Sprite id was nil.") end
        id = type(id) == "number" and id or sheetInfo:getFrameIndex(id)
        local sprite = display.newImage(imageSheet, id)
        params = params or {}

        return sprite
    end
    
    sheet:init()
    return sheet
end

return SpriteSheet
local Canceler = {}

function Canceler:new()
	local canceler = {}
	local t = {}

	function canceler:add(tweenOrTrigger)
		table.insert(t, tweenOrTrigger)
	end	

	function canceler:clear()
		for k, v in pairs(t) do
			if v and v.cancel then 
				v:cancel()
			else
				transition.cancel(v)
			end
			v = nil
		end
		t = {}
	end

	function canceler:destroy()
		self:clear()
		t = nil
		canceler = nil
	end

	return canceler
end

return Canceler
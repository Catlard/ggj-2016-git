local Colors = {}


local function hexToRGB(hexCode)
   assert(#hexCode == 7, "The hex value must be passed in the form of #XXXXXX");
   local hexCode = hexCode:gsub("#","")
   return {tonumber("0x"..hexCode:sub(1,2))/255,tonumber("0x"..hexCode:sub(3,4))/255,tonumber("0x"..hexCode:sub(5,6))/255};
end

function Colors:getRandom(hasAlpha)
	return {
		math.random(), --r
		math.random(), --g
		math.random(), --b
		hasAlpha and math.random() or 1 --a?
	} 
end

function Colors:multiply(colorA, colorB)
	local new = copy(colorA)
	if type(colorB) == "number" then
		colorB = {colorB,colorB,colorB}
	end
	for i = 1, #new do
		new[i] = new[i] * colorB[i]
	end
	return new
end

--pastels
pastels = {} 
pastels.red    = hexToRGB("#FACDCD")
pastels.yellow = hexToRGB("#F8FACD")
pastels.green  = hexToRGB("#D2FACD")
pastels.blue   = hexToRGB("#CDFAEC")
pastels.purple = hexToRGB("#ECCDFA")
pastels.gray   = {.33,.33,.33}
Colors.pastels = pastels

--light brights - http://www.color-hex.com/color-palette/5083
brights = {}
brights.red     = hexToRGB("#FF6698")
brights.orange  = hexToRGB("#FFB366")
brights.yellow  = hexToRGB("#FFFF66")
brights.green   = hexToRGB("#98FF66")
brights.blue    = hexToRGB("#6698FF")
Colors.lightBrights = brights



--absolutes

Colors.white  = {1,1,1}
Colors.gray   = {.5,.5,.5}
Colors.black  = {0,0,0}
Colors.red    = hexToRGB("#D10000")
Colors.yellow = hexToRGB("#E8E515")
Colors.tan    = hexToRGB("#FCD58B")
Colors.brown  = hexToRGB("#7A5932")




return Colors
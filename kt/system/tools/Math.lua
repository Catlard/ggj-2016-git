function math.clamp(val, lower, upper)
    assert(val and lower and upper, "Math.clamp is missing some valid argument.")
    if lower > upper then lower, upper = upper, lower end -- swap if boundaries supplied the wrong way
    return math.max(lower, math.min(upper, val))
end

function math.lerp( v0, v1, t )
    return v0 + (t * (v1 - v0))
end

function math.inBounds(x, y, bounds)
	local b = bounds

	if x > b.xMax or x < b.xMin then
		return false
	elseif y > b.yMax or y < b.yMin then
		return false
	else return true end
end

function getUnitCirclePosition(angle, radius)
	local radians = math.rad (angle)
	local radius = radius or 1
	return math.cos(radians) * radius, math.sin(radians) * radius
end
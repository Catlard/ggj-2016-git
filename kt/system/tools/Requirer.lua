
function load(className)
	local class = require(className)
	if type(class) == "table" then
		class._ktName = className
	end
	return class
end

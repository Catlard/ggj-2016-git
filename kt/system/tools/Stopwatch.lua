local Stopwatch = {}

function Stopwatch:new(params)

	local stopwatch = {}

	local since = system.getTimer() -- the clock starts NOW.
	stopwatch.timeCount = 0
	local isCountingDown = false
	local isStopped = false


	if params.timeRemaining then 
		stopwatch.timeCount = params.timeRemaining 
		isCountingDown = true
	end


	local function getDeltaTime()
	    local temp = system.getTimer()  -- Get current game time in ms
	    local dt = temp-since
	    since = temp  -- Store game time
	    --print(dt)
	    return dt
	end

	local function everyFrame()

		--if we're not running the clock, then always cancel out delta time and break out.
		if isStopped then 
			since = system.getTimer() 
			return
		end

		if params.updateAction then
			params.updateAction(stopwatch.timeCount)
		end

		if isCountingDown then
			if stopwatch.timeCount > 0 then
				--print(stopwatch.timeCount)
				stopwatch.timeCount = stopwatch.timeCount - getDeltaTime()
			elseif stopwatch.timeCount <= 0 then
				if params.finishAction then params.finishAction() end
				stopwatch.timeCount = 0
				stopwatch:stop()
			end
		else
			stopwatch.timeCount = stopwatch.timeCount + getDeltaTime()
		end
	end

	function stopwatch:start()
		since = system.getTimer()
		isStopped = false
	end

	function stopwatch:stop()
		isStopped = true
	end

	function stopwatch:destroy()
		self:stop()
		Runtime:removeEventListener("enterFrame", everyFrame)
		cleanGroup(stopwatch)
		stopwatch = nil
	end


	Runtime:addEventListener("enterFrame", everyFrame)

	return stopwatch
end



return Stopwatch
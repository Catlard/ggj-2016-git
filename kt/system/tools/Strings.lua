-- this stuff from LUAforth http://angg.twu.net/LUA/luaforth.lua
--

lstrip = function (str) return str:match("^[ \t]*(.*)$") end
 strip = function (str) return str:match("^[ \t]*(.-)[ \t]*$") end
rstrip = function (str) return str:match(      "^(.-)[ \t]*$") end
string.lstrip = lstrip
string.strip  =  strip
string.rstrip = rstrip

zfill = function(str, digits) 
    return string.format("%0" .. digits .. ".0f", str);
end

function startswith(sbig, slittle)
    return string.sub(sbig, 1, string.len(slittle)) == slittle
end

function endswith(sbig, slittle)
    local sll=string.len(slittle)
    local sbl=string.len(sbig)
    return string.sub(sbig, sbl-sll+1) == slittle
end

-- works with UTF-8
function string.reverse(s)
    local out=""
    for _, v in utfChars(s) do
        out=v..out
    end
    return out
end


-- from http://lua-users.org/wiki/SplitJoin
-- but I swapped the parameter order round

-- Concat the contents of the parameter list,
-- separated by the string delimiter (just like in perl)
-- example: strjoin(", ", {"Anna", "Bob", "Charlie", "Dolores"})
function string.join(list, delimiter)
  local len = getn(list)
  if len == 0 then 
    return "" 
  end
  local string = list[1]
  for i = 2, len do 
    string = string .. delimiter .. list[i] 
  end
  return string
end

-- Split text into a list consisting of the strings in text,
-- separated by strings matching delimiter (which may be a pattern). 
-- example: strsplit(",%s*", "Anna, Bob, Charlie,Dolores")
function string.split(text, delimiter, plain)
  local strfind = string.find
  local strsub= string.sub
  local list = {}
  local pos = 1
  if strfind("", delimiter, 1) then -- this would result in endless loops
    error("delimiter matches empty string!")
  end
  while 1 do
    local first, last = strfind(text, delimiter, pos, 100, plain)
    if first then -- found?
      table.insert(list, strsub(text, pos, first-1))
      pos = last+1
    else
      table.insert(list, strsub(text, pos))
      break
    end
  end
  return list
end


function string.capitalize(str)
    local a = string.upper(string.sub(str,1,1))
    local b = string.sub(str, 2)
    return a .. b
end

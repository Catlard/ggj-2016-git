function shuffle(array)
    local n, random, j = #array, math.random
    for i=1, n do
        j,k = random(n), random(n)
        array[j],array[k] = array[k],array[j]
    end
    return array
end

function contains(array, item)
	for i = 1, #array do
		if array[i] == item then return true end
	end

	return false
end

function copy(t, params)

    local u={}

    if not params then params={} end

    if not params.noIndex then
        for i=1,#t do
            table.insert(u, t[i])
        end
    end

    if not params.noPairs then 
        for k, v in pairs(t) do
            u[k] = v
        end
    end

    return u
end

function deepcopy(object)
    local lookup_table = {}
    local function _copy(object)
        if type(object) ~= "table" then
            return object
        elseif lookup_table[object] then
            return lookup_table[object]
        end
        local new_table = {}
        lookup_table[object] = new_table
        for index, value in pairs(object) do
            new_table[_copy(index)] = _copy(value)
        end
        return setmetatable(new_table, getmetatable(object))
    end
    return _copy(object)
end

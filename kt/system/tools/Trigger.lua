local Trigger={}

function Trigger:new()

    local trigger = {}
    
    local triggers = {}

    function trigger.create(delay, onComplete, iterations)
 
        local listener={}

        function listener:timer()
            listener.t:remove()
            onComplete()
        end

        local t = timer.performWithDelay(delay, listener, iterations)
        listener.t = t

        function t:remove()
            --printf("trigger.remove with %d triggers active", #triggers)
            for k ,v in pairs(triggers) do
                if v == t then
                    --print ("found this trigger in the list")
                    triggers[k]=nil
                end
            end
        end

        function t:cancel()
            --print "cancelling a trigger"
            t:remove()
            timer.cancel(t)
        end
        
        table.insert(triggers, t)

        return t
    end

    function trigger.pauseAll()
        for _, v in pairs(triggers) do
            timer.pause(v)
        end
    end

    function trigger.resumeAll()
        for _, v in pairs(triggers) do
            timer.resume(v)
        end
    end

    function trigger.cancelAll()
        for k, v in pairs(triggers) do
            timer.cancel(v)
            triggers[k]=nil
        end
    end

    function trigger.listAll()
        print( "********** START LIST OF TRIGGERS **********\n" )
        local count=0
        for _, v in pairs(triggers) do
            count=count+1
        end
        print ("Total trigger count = " .. count)
    end

    return trigger

end

return Trigger


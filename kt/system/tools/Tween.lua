local Tween={}

function Tween:new()

    local tween={}

    function tween.create(object, params)
        params.tag = 'game'
        local t = transition.to(object, params)

        function t:cancel()
            transition.cancel(t)
        end

        return t
    end
     
    function tween.pauseAll()
        transition.pause('game')
    end
     
    function tween.resumeAll()
        transition.resume('game')
    end
     
    function tween.cancelAll()
        transition.cancel('game')
    end
     
    return tween
end

return Tween


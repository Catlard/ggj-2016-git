local Help = {}

function Help:new()

	local view = display.newGroup()

	function view:init()
		local Help = Sheets:getAnimatedSprite("Help", {
			name = "help",
			start = 1,
			count = 2,
			time = 1000,
			loopCount = 0,
			loopDirection = "forward" -- "bounce"
		})
		Help:setSequence("help")
		Help:play()
		Help:scale(.5,.5)
		view:insert(Help)
	end



	return view





end


return Help
local King = {}

function King:new()

	local view = display.newGroup()

	function view:init()
		local King = Sheets:getAnimatedSprite("King", {
			name = "king",
			start = 1,
			count = 2,
			time = 1000,
			loopCount = 0,
			loopDirection = "forward" -- "bounce"
		})
		King:setSequence("king")
		King:play()
		King:scale(.5,.5)
		view:insert(King)
	end



	return view





end


return King
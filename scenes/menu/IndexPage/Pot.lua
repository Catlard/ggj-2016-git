local Pot = {}

function Pot:new()

	local view = display.newGroup()

	function view:init()
		local Pot = Sheets:getAnimatedSprite("Pot", {
			name = "Pot",
			start = 1,
			count = 3,
			time = 1000,
			loopCount = 0,
			loopDirection = "forward" -- "bounce"
		})
		Pot:setSequence("POT")
		Pot:play()
		Pot:scale(.5,.5)
		view:insert(Pot)
	end



	return view





end


return Pot
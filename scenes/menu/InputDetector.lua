local InputDetector = {}

function InputDetector:new()

	local detector = {}
	local playerKeys = {}

	local function onKey(event)
		if event.phase == "down" then
			local isThere
			local playerCount = 1
			for k, v in pairs(playerKeys) do
				-- print(k .. " vs: " .. event.descriptor)
				playerCount = playerCount + 1 
				if k == event.descriptor then 
					isThere=true 
				end 
			end

			if isThere then
				playerKeys[event.descriptor] = nil
			else
				playerKeys[event.descriptor] = {math.random(), math.random(), math.random()}
			end
			
			if not isThere then 
				print("DETECTED PLAYER: " .. playerCount .. ", " .. event.descriptor)
			end

			Controller:playersUpdated(playerKeys) 


		end
	end

	function detector:init()
		Runtime:addEventListener("key", onKey)

	end

	function detector:destroy()
		Runtime:removeEventListener("key", onKey)
		detector = nil
	end



	return detector

end




return InputDetector
local Controller = {}

function Controller:new()
	local controller = {}
	
	function controller:init() -- starting the game
		Model:init()
		View:init()
	end

	function controller:playersUpdated(keys)

		Model:setNewPlayers(keys)

		tellViews("resetCountDown")
		if Model:enoughPlayersToPlay() then tellViews("startCountdown") end
		tellViews("showCurrentPlayers", keys)
	end

	function controller:playersReady()
		local players = Model:getPlayers()
		Director:changeScene{
			scene = "scenes.test.Test",
			payload = Model:convertPlayers(players)
		}

	end


	function controller:destroy()
		View:destroy()
		Model:destroy()
	end

	return controller
end

return Controller
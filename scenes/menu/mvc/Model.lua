local Model = {}

function Model:new()
	local model = {}

	local currentPlayers
	local playerCount = 0
	local currentGod

	function model:init()
		
	end


	function model:convertPlayers(players)
		local safe = {}
		for k, v in pairs(players) do
			local pT = {}
			pT[k] = v
			table.insert(safe, pT)
		end

		return safe


	end



	

	function model:setNewPlayers(players)
		currentPlayers = deepcopy(players)
	end

	function model:enoughPlayersToPlay()
		playerCount = 0
		if currentPlayers then
			for k, v in pairs(currentPlayers) do
				playerCount = playerCount + 1
			end
		end
		return playerCount > 1
	end


	function model:getPlayers()
		return currentPlayers
	end

	function model:destroy()
	end

	return model
end

return Model
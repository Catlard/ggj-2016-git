local View = {}

function View:new()

	local view = display.newGroup()
	local countDownLayer
	local playerLayer 
	local canceler = Canceler:new()
	local currentCount
	local secondBeforeEnteringGame = 3
	local inputDetector
	local music

	local King = require("scenes.menu.indexPage.King"):new()
	Sheets:add("King", "assets.art.indexPage.king")
	local Help = require("scenes.menu.indexPage.Help"):new()
	Sheets:add("Help", "assets.art.indexPage.help")
	local Pot = require("scenes.menu.indexPage.Pot"):new()
	Sheets:add("Pot", "assets.art.indexPage.pot")

	function view:startCountdown()
		canceler:add(Trigger.create(750, function()
			view:startCountdown()
			currentCount = currentCount - 1
			if currentCount < 0 then
				Controller:playersReady()
			else
				view:showTime(currentCount)
			end
		end))
	end

	function view:init()
		logViewIn("menuView", view)
 		print "hello"
		music = SFX:play({name = "music/titleMusic", volume = .5},{ loops = 0})


		local bg = display.newImage("assets/art/indexPage/bk.png", true)
		bg:scale(.7,.7)
		view:insert(bg)



		countDownLayer  = display.newGroup()
		playerLayer = display.newGroup()
		view:insert(countDownLayer)
		

		inputDetector = require('scenes.menu.inputDetector'):new()
		inputDetector:init()

		King:init()
		King.x = -160
		King.y = 25
		King:scale(1.3,1.3)
		view:insert(King)

		Pot:init()
		Pot.x = 250
		Pot.y = 140
		Pot:scale(1.3,1.3)
		view:insert(Pot)

		Help:init()
		Help.x = 250
		Help.y = -85
		Help:scale(1.3,1.3)
		view:insert(Help)

		local ui = display.newImage("assets/art/indexPage/UI.png", true)
		ui:scale(.7,.2)
		ui.y = 200
		view:insert(ui)
		
		local gametitle = display.newImage("assets/art/indexPage/title.png", true)
		gametitle:scale(.7,.7)
		gametitle.y = -150
		view:insert(gametitle)

		view:insert(playerLayer)
	end

	function view:showTime(time)
		cleanGroup(countDownLayer,true)
		local options = {
		    text = "遊戲開始倒數 Time until start: " .. time,     
		    fontSize = 26,
		    width = 200,
		    align="center"
		}
		local myText = display.newText( options )
		myText.x = 50
		myText.y = 0
		countDownLayer:insert(myText)
		myText:setFillColor(1,1,1,1)
	end


	function view:resetCountDown()
		canceler:clear()
		currentCount = secondBeforeEnteringGame
		view:showTime(currentCount)
	end

	local playerBoxWidth = 40
	local spacing = 5
	local psY = 200
	local psX
	local randomColors = {}
	for i = 1, 100 do
		table.insert(randomColors, {math.random(), math.random(), math.random(), 1})
	end

	local function showPlayer(ndx, playerName)
		local pGroup = display.newGroup()
		pGroup.x, pGroup.y = ((ndx-1) * (playerBoxWidth + spacing)) + psX, psY
		local r = display.newRoundedRect(0,0,playerBoxWidth, playerBoxWidth, 5)
		r:setFillColor(unpack(randomColors[ndx]))
		pGroup:insert(r)

		local options = {
		    text = string.sub(playerName, 1, 2),     
		    fontSize = 14,
		    align = "center"
		}
		local myText = display.newText( options )
		countDownLayer:insert(myText)
		myText:setFillColor(1,1,1,1)
		pGroup:insert(myText)

		playerLayer:insert(pGroup)

	end


	function view:showCurrentPlayers(players)
		cleanGroup(playerLayer, true)
		local sortedPlayers = {}
		for k, v in pairs(players) do
			table.insert(sortedPlayers, k)
		end	


		psX = ((#sortedPlayers-1) * -playerBoxWidth/2) + 0

		table.sort(sortedPlayers, function(a,b)
			return a < b
		end)

		for i = 1, #sortedPlayers do
			showPlayer(i, sortedPlayers[i])
		end	



	end


	function view:destroy()
		if music then
			music.stop();
			music = nil;
		end
		cleanGroup(view); view = nil
		canceler:destroy(); canceler = nil
		logViewOut("menuView")
		inputDetector:destroy(); inputDetector= nil
	end

	return view
end

return View

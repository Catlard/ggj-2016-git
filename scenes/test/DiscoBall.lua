local DiscoBall = {}

function DiscoBall:new()

	local view = display.newGroup()

	function view:init()
		local ball = Sheets:getAnimatedSprite("discoBall", {
			name = "spin",
			start = 1,
			count = 5,
			time = 1000,
			loopCount = 0,
			loopDirection = "forward" -- "bounce"
		})
		ball:setSequence("spin")
		ball:play()
		ball:scale(.5,.5)
		view:insert(ball)
	end



	return view





end


return DiscoBall
local Doll = {}





function Doll:new(params)
	
	params = params or {}
	params.speed = params.speed or 1
	local countdownCanceler = Canceler:new()
	local countdownLayer, boomLayer, partLayer
	local deathCanceler = Canceler:new()

	local view = display.newGroup()
	local parts = {}
	local armDist = -15
	local armHeight = ({A = 55, king = 50, C = 40})[params.sheet] or 45
	local legDist = 20
	local legHeight = 125
	local headHeight = ({B = -25})[params.sheet] or -46
	local torsoHeight = 85
	local torsoX = 2
	local headX = ({king = -12})[params.sheet] or 0
	local partLayerScale = ({A = 1.2, B = 1.2})[params.sheet] or 1


	local jointAlpha = 0

	local armAngleRange = 110
	local legAngleRange = 110
	parts.maxPoints = (armAngleRange * 2) + (legAngleRange * 2)
	local angleTime = 2000
	local halfTime = angleTime/2
	local lerpPercent = .01
	local randomTimeOffset = math.random(angleTime)
	local startTime = system.getTimer()


	--if it won't be rotated, must be removed from list in set parts to
	local partParams = {
		{ partName = "lArm", x= -armDist, y = armHeight, anchorX = 0, anchorY = 0, aMin = 90 - armAngleRange, aMax = 90},
		{ partName = "rArm", x= armDist, y = armHeight, anchorX = 1, anchorY = 0, aMin = -90, aMax = -90 + armAngleRange},
		{ partName = "lLeg", x= legDist, y = legHeight, anchorX = .3, anchorY = 0,aMin = -90, aMax = -90 + legAngleRange },
		{ partName = "rLeg", x= -legDist, y = legHeight, anchorX = .7, anchorY = 0, aMin = 90 - legAngleRange, aMax = 90},
		{ partName = "torso", x = torsoX, y = torsoHeight},
		{ partName = "head", x = headX, y = headHeight},
	}

	local partsAuthorizedToMove


	function view:getPartsTable()
		return parts
	end

	function view:cleanCountdown()
		cleanGroup(countdownLayer, true)
		countdownCanceler:clear()
	end


	local prev
	function view:countDownFrom(num)
		local toWorkWith = Model:getGodRoundTime()/7


		local function showNumber(n)
			local spriteName = "num_"..n
			local s
			if n == 0 then
				s = Sheets:getSprite("num", "num_stop")
				SFX:play({name = "sfx/time01", volume = .5},{ loops = 0})
			else
				view:countDownFrom(n-1)
				s = Sheets:getSprite("num", spriteName)
				SFX:play({name = "sfx/da_007", volume = .5},{ loops = 0})
			end
			s.alpha = 0
			countdownCanceler:add(Tween.create(s, {
				alpha = 1,
				delay = toWorkWith * .6,
				time = toWorkWith * .4,
				onComplete = function()
					if n == 0 then return end
					if prev then prev.alpha = 0 end
				end

			}))
			prev = s

			countdownLayer:insert(s)
			countdownLayer:toFront()
			s:scale(.5,.5)

		end

		countdownCanceler:add(Trigger.create(toWorkWith, function()
			showNumber(num)

		end))
	end	

	function view:getEaten(callback)
		local deathTime =  Model:getDeathTime()/2
		deathCanceler:add(Tween.create(partLayer, {
			time = deathTime * .33,
			alpha = .5,
			xScale = .01,
			yScale = .01,
			transition = easing.inBack,
		}))
		deathCanceler:add(Trigger.create(deathTime * .2, function()
			self:makeBoomMeat()
		end))

		deathCanceler:add(Tween.create(boomLayer, {
			delay = deathTime * .3,
			time = deathTime * .5,
			x=0,
			y=150,
			transition = easing.inCirc,
			onComplete = function()
				-- local par = view.parent
				-- view.parent = nil
				-- deathCanceler:add(Tween.create(view, {
				-- 	0,0,
				-- 	time = deathTime,
				-- 	transition = easing.inOutBack,
				-- }))
				-- par:insert(view)
				deathCanceler:add(Tween.create(boomLayer, {
					delay = deathTime/2,
					time = deathTime/2,
					xScale = .01,
					yScale = .01,
					transition = easing.inBack,
				}))
			end
		}))

		deathCanceler:add(Trigger.create(deathTime * 2, function()
			callback()
		end))
	end	

	function view:makeBoomMeat()
		local boom = Sheets:getAnimatedSprite("boom", {
			name = "die",
			start = 1,
			count = 4,
			time = 1000,
			loopCount = 1,
			loopDirection = "forward" -- "bounce"
		})
		boom.y = -5

		boom:setSequence("die")
		boom:play()

		local boomScale = math.random(100, 120)/100
		boom:scale(boomScale,boomScale)
		boomLayer:insert(boom)
		SFX:play({name = "sfx/die01", volume = .5},{ loops = 0})
	end


	function view:setPartsToMove(numberOfParts)
		numberOfParts = numberOfParts or 0
		partsAuthorizedToMove = {}
		local safeParts = deepcopy(partParams)
		safeParts[5] = nil
		safeParts[6] = nil
		shuffle(safeParts)

		for i = 1, numberOfParts do
			table.insert(partsAuthorizedToMove, table.remove(safeParts))
		end

	end

	function view:highlightPart(partName)
		for k, v in pairs(parts) do
			if k == partName then
				v:setFillColor(1,0,0)
			elseif type(v) == "table" then
				v:setFillColor(1,1,1)
			end
		end

	end

	function view:movePart(partName)
		for k, v in pairs(parts) do
			if k == partName then
				v.moveDirection = v.moveDirection * -1
				partsAuthorizedToMove = {deepcopy(v)}

			end
		end
	end

	local prevTime = 0
	local canMove = true

	function view:setCanMove(state)
		canMove = state
	end

	local pausedTime = 0
	local prev

	local function onEnterFrame(event)
		if not canMove then 
			pausedTime = pausedTime + (system.getTimer() - (prev or system.getTimer()))
			prev = system.getTimer()
			return 
		end

		local timeSinceStart = ((system.getTimer() - pausedTime - startTime ) * params.speed / 10)
		local deltaTime = timeSinceStart - prevTime

		if partsAuthorizedToMove then
			for i = 1, #partsAuthorizedToMove do
				local v = parts[partsAuthorizedToMove[i].partName]

				if v.aMin then
					-- local currTime = (timeSinceStart + randomTimeOffset) % angleTime 
					-- if currTime > halfTime then 
					-- 	currTime = halfTime - (currTime-halfTime)
					-- end
					-- v.movePercent = math.lerp(v.aMin, v.aMax, currTime/halfTime)
					
					local newRot = v.rotation + (v.moveDirection * deltaTime)
					if newRot > v.aMax then v.moveDirection =  -1
					elseif newRot < v.aMin then v.moveDirection = 1
					end 

					--print(newRot, v.moveDirection, v.aMin, v.aMax, deltaTime)

					v.rotation = newRot -- math.lerp(v.rotation, newRot, lerpPercent)
				end
			end
		end

		prevTime = timeSinceStart
	end

	local function makePart(params, isGlow)
		local suffix = isGlow and "_grow" or ""
		local spriteName = params.sheet.."_"..params.partName..suffix
		print(spriteName)

		local part = Sheets:getSprite(params.sheet, spriteName)
		
		parts[params.partName] = part
		part.partName = params.partName
		part.moveDirection = math.random() > .5 and 1 or -1
		part.movePercent = math.random()

		part.anchorX, part.anchorY = params.anchorX or .5, params.anchorY or .5
		part.x, part.y = params.x, params.y
		part:scale(.2,.2)
		part.aMin, part.aMax = params.aMin, params.aMax
		partLayer:insert(part)
		
		return part

		-- local r = display.newRect(params.x, params.y, 10,10)
		-- r:setFillColor(0,1,0,.5)
		-- view:insert(r)
		
	end

	function view:init()

		countdownLayer = display.newGroup()
		partLayer = display.newGroup()
		partLayer:scale(partLayerScale, partLayerScale)

		for i = 1, #partParams do
			local p = deepcopy(partParams[i])
			p.sheet = params.sheet
			local part = makePart(p)
			-- if p.aMin then
			-- 	local glow = makePart(p, true)
			-- 	part.glow = glow
			-- end
		end

		view:scale(.8,.8)

		view:insert(countdownLayer)
		countdownLayer.x = 60
		countdownLayer.y = -90
		boomLayer = display.newGroup()
		

		if params.isPlayer then
			local plat = display.newImage("assets/art/wow.png", true)
			plat:scale(1.95,1.95)
			plat.y = 250
			plat.x = -25
			view:insert(plat)
			if params.side == "right" then
				plat.x = plat.x * -1
				plat.xScale = plat.xScale * -1

			end
		end

				view:insert(partLayer)

		view:insert(boomLayer)
		Runtime:addEventListener("enterFrame", onEnterFrame)

	end

	function view:destroy()
		deathCanceler:destroy();deathCanceler = nil
		countdownCanceler:destroy(); countdownCanceler=nil
				cleanGroup(countdownLayer)

		Runtime:removeEventListener("enterFrame", onEnterFrame)
		cleanGroup(view); view = nil
		partParams = nil
	end





	return view
end





return Doll
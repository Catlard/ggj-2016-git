local HumanLeader = {}

function HumanLeader:new()
	local view = display.newGroup()

	local movableParts = Model:getMovableParts()
	local announcementLayer = display.newGroup()
	local announcementButton

	local currentMovableNdx = math.random(#movableParts)
	local longPartNames = {
		rArm = "左手  Left arm",
		lArm = "右手  Right arm",
		rLeg = "左腳  Left leg",
		lLeg = "右腳  Right leg",
	}
	local movablePartCanceler = Canceler:new()

	function view:changeMovablePart()
		currentMovableNdx = currentMovableNdx+1
		if currentMovableNdx > #movableParts then
			currentMovableNdx = 1
		end
	end

	function view:changeAnnouncement()
		view:changeMovablePart()
		if announcementButton then announcementButton:destroy(); announcementButton = nil end
		cleanGroup(announcementLayer, true)
		local newAnnouncement = movableParts[currentMovableNdx]
		Controller:newLeaderAnnouncement(newAnnouncement)
		--print(newAnnouncement.. "   WAS ANNOUNCEMENT")
		-- announcementButton = Button:new{
		-- 	text = longPartNames[newAnnouncement],
		-- 	width = 160
		-- }
		-- view:insert(announcementButton)
		local time = Model:getAnnouncementChangeTime()
		
		local function moveIt()
			view:changeAnnouncement()
		end

		movablePartCanceler:add(Trigger.create(time, moveIt))


	end

	function view:startChangingMovableParts()
		view:changeAnnouncement()
	end

	function view:stopChangingMovableParts()
		movablePartCanceler:clear()
	end



	function view:init()
		logViewIn("humanLeader", view)
	end

	function view:destroy()
		movablePartCanceler:destroy()
	end


	return view

end


return HumanLeader
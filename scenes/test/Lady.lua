local stove = {}

function stove:new()

	local view = display.newGroup()

	function view:init()
		local lady = Sheets:getAnimatedSprite("stove", {
			name = "lady",
			start = 1,
			count = 2,
			time = 1000,
			loopCount = 0,
			loopDirection = "forward" -- "bounce"
		})
		lady:setSequence("lady")
		lady:play()
		lady:scale(.5,.5)
		lady.y = 60
		view:insert(lady)
	end



	return view





end


return DiscoBall
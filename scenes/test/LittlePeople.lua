local LittlePeople = {}

function LittlePeople:new()
	local view = display.newGroup()
	function view:init()
		local LittlePeople = Sheets:getAnimatedSprite("LittlePeople", {
			name = "littlepeople",
			start = 1,
			count = 2,
			time = 1000,
			loopCount = 0,
			loopDirection = "forward" -- "bounce"
		})
		LittlePeople:setSequence("littlepeople")
		LittlePeople:play()
		LittlePeople:scale(.5,.5)
		view:insert(LittlePeople)
	end
	return view
end
return LittlePeople
local MusicMixer = {}

function MusicMixer:new()
	local mixer = {}
	local musicIntensity = 0
	local musicLayers
	local startMusicVolume = .4
	local musicDegredation = .4
	local minimumVolume = math.pow(.5,4)
	local winMusic


	local base = "music/gxMusic/"
	local playingTracks = {}

	local gxMusic = {
		{
			"C_Bass",
			"C_Kick&Snare",
			"C_Percussion"
		},

		{
			-- "brian",
			-- "simon",
			"C_Vocal_Blue&Brian",
			"C_Vocal_Simon",
		},

		{
			"C_Vocal_Ethan",
			"C_Vocal_Hawk",
			"C_Vocal_Baibai",

		}
	}

	local safeTracks

	-- local function onKey(event)
	-- 	if event.descriptor == "o" and event.phase == "down" then
	-- 		mixer:addMusicIntensity()
	-- 	end
	-- end	

	


	function mixer:addMusicIntensity()
		local musicMod = (musicIntensity % #safeTracks) + 1
		dump(safeTracks[musicMod])
		safeTracks[musicMod].used = (safeTracks[musicMod].used or 0) + 1
		local category = safeTracks[musicMod]

		if safeTracks[musicMod].used > #category then
			print("MOre tracks than existed in mod " .. musicMod)
			return
		else
			local chosen = safeTracks[musicMod][safeTracks[musicMod].used]
			for i = 1, #playingTracks do
				playingTracks[i].volume = playingTracks[i].volume*musicDegredation
				if playingTracks[i].volume < minimumVolume then
					playingTracks[i].volume = 0
				end
				audio.setVolume(playingTracks[i].volume, {source= playingTracks.source})
			end	

			local sound = SFX:play({name = base..chosen, volume = startMusicVolume},{ loops = -1})
			sound.volume = startMusicVolume
			sound.name = chosen
			table.insert(playingTracks, sound)
			musicIntensity = musicIntensity + 1
		end

	end

	function mixer:stopMusic()
		if winMusic then winMusic.stop(); winMusic = nil end
		if not playingTracks then return end
		print("TRIED TO STOP MUSIC!")
		dump(playingTracks)
		for i = 1, #playingTracks do
			dump(playingTracks[i])
			print("TRIED TO STOP THIS ONE!")
			playingTracks[i].stop()
			playingTracks[i] = nil
		end
		playingTracks = {}
	end

	function mixer:resetMix()
		musicLayers = {}
		self:stopMusic()
		safeTracks = deepcopy(gxMusic)
		for i = 1, #safeTracks do
			safeTracks[i] = shuffle(safeTracks[i])
			safeTracks[i].used = 0
		end	
	end


	function mixer:playWinMusic()
		mixer:stopMusic()
		winMusic = SFX:play({name = base.."C-Gameplay_ArrangedMusic", volume = .4},{ loops = 1 })
	end

	function mixer:init()
		logViewIn("mixer", mixer)
		self:resetMix()

		--Runtime:addEventListener("key", onKey)
	end

	function mixer:destroy()
		mixer:resetMix()
		logViewOut("mixer")
		mixer = nil
	end


	return mixer
end



return MusicMixer
local PlayerBox = {}

function PlayerBox:new(params)
	local view = display.newGroup()
	local myKeyDescriptor = "none"
	local canGetInput = false
	local partCurrentlyMoving
	local isKeyDown = false
	local rect, doll, myText
	local scoreLayer
	local currentScore = 100
	local playerName = "playerBox"..math.random(9999999)
	local deathCanceler = Canceler:new()
	local customColor = {1,.5,.5,1}
	local scoreCanceler = Canceler:new()
	local canResetGame = false



	function view:getCustomColor()
		return customColor
	end

	function view:getCustomDescriptor()
		return myKeyDescriptor
	end


	
	function view:showScore()
		cleanGroup(scoreLayer,true)
		local options = {
		    text = currentScore, --params.ndx,     
		    fontSize = 25,
		    align = "center"
		}

		local r = display.newRoundedRect(0,0,60, 40, 5)
		r.strokeWidth = 3
		r:setFillColor(unpack(customColor))
		scoreLayer:insert(r)

		local scoreText = display.newText( options )
		scoreText:setFillColor(1,1,1,1)
		scoreLayer.xScale, scoreLayer.yScale = .01,.01
		local lighter = deepcopy(customColor)
		for i = 1, #lighter do
			lighter[i] = lighter[i] * .5
		end



		scoreCanceler:add(Tween.create(scoreLayer, {
			time = Model:getScoreTime()/4,
			xScale = 1,
			yScale = 1,
			transition = easing.outBack
		}))


		scoreLayer:insert(scoreText)
	end

	function view:hideScore()
		cleanGroup(scoreLayer,true)
	end






	local function onKey(event)
		if canResetGame then Controller:resetGame() end

		if not canGetInput then return end
		if event.descriptor ~= myKeyDescriptor then return end
		if event.phase == "down"  and isKeyDown ==false then
			partCurrentlyMoving = Model:getLeaderAnnouncement()
			--print("PUSHED " .. myKeyDescriptor..", moving " .. partCurrentlyMoving)
			isKeyDown = true
			doll:movePart(partCurrentlyMoving)
		elseif event.phase == "up" then
			doll:setPartsToMove(0)
			isKeyDown = false
			--print("SCORE FOR PLAYER WAS:", currentScore)
		end
	end

	local function onEnterFrame(event)
			currentScore = view:getCurrentScore()
	end

	function view:getEaten()
		doll:getEaten(function()
			self:destroy()
		end)
	end

	function view:highlightPart(announcement)
		doll:highlightPart(announcement)

	end


	function view:getCurrentScore()
		return Model:getPlayerScore(doll:getPartsTable())
	end

	function view:showWinner()
		canResetGame = true
	end

	function view:init()
		
		logViewIn(playerName, view)
		Runtime:addEventListener("key", onKey)
		Runtime:addEventListener("enterFrame", onEnterFrame)
		for k, v in pairs(params.inputKey) do
			myKeyDescriptor = k
			customColor = type(v) == "table" and v or customColor
			dump(params)
			dump(customColor)
			print("WAS DESCRIPTOR")
		end
		print("KEY DESCRIPTOR: " .. myKeyDescriptor)

		local randomColor = {math.random(), math.random(), math.random(), 0}

		rect = display.newRoundedRect(params.x or 0, params.y or 0, params.w or 10, params.h or 10, 5)
		rect:setFillColor(unpack(randomColor))
		view:insert(rect)



		doll = require("scenes.test.Doll"):new{side = params.side, isPlayer = true, speed = Model:getDollSpeed(), sheet = params.sheet}
		doll:init()
		doll:scale(.5,.5)
		view:insert(doll)
		doll.y = -30


		local r = display.newRoundedRect(0,0, 35,35, 5)
		r.strokeWidth = 3
		view:insert(r)
		r:setFillColor(unpack(customColor))


		local options = {
		    text = string.sub(myKeyDescriptor, 1, 2), --params.ndx,     
		    fontSize = 22,
		}
		myText = display.newText( options )
		view:insert(myText)
		myText:setFillColor(1,1,1,1)
		myText.x = 60
		myText.y = -50


		if params.side == "left" then
			myText.x = myText.x * -1
		end

		r.x, r.y = myText.x, myText.y

		scoreLayer = display.newGroup()
		view:insert(scoreLayer)

	end	

	function view:enablePlayerMovement()
		canGetInput = true
	end

	function view:disablePlayerMovement()
		canGetInput = false
		doll:setPartsToMove(0)
	end


	function view:destroy()
		scoreCanceler:destroy(); scoreCanceler = nil
		doll:destroy(); doll = nil
		Runtime:removeEventListener("enterFrame", onEnterFrame)
		Runtime:removeEventListener("key", onKey)
		logViewOut(playerName)
		cleanGroup(view); view = nil
	end


	return view
end


return PlayerBox
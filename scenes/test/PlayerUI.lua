local PlayerUI = {}

function PlayerUI:new()

	local view = display.newGroup()
	local PlayerBox = require('scenes.test.PlayerBox')

	local numPlayers
	local currentPlayers

	local boxWidth = my.width * .2


	local boxesPerSide 
	local playerBoxes = {
		leftSide = {},
		rightSide = {}
	}

	function view:killWorstPlayer()
		local lowestScore = 100
		local highestScore = 0
		local worstPlayer
		local bestPlayer
		local ndxToKill = 1
		print("FOUND PLAYERS:" .. #currentPlayers)
		for i = 1, #currentPlayers do
			local p = currentPlayers[i]
			local s = p:getCurrentScore()
			if s < lowestScore then
				lowestScore = s
				worstPlayer = p
				ndxToKill = i
			end
			if s >= highestScore then
				highestScore = s
				bestPlayer = p
			end
		end
		local safePlayers = {}
		for i = 1, #currentPlayers do
			if i ~= ndxToKill then
				table.insert(safePlayers, currentPlayers[i])
			end
		end
		Model:setCurrentWinner(bestPlayer)
		currentPlayers = safePlayers
		print("WORST PLAYER WAS:::::::")
		print(lowestScore,ndxToKill)
		worstPlayer:getEaten()
		--worstPlayer:destroy(); worstPlayer = nil
		
	end
	
	function view:init()
		currentPlayers = {}
		logViewIn("playerUI", view)
		numPlayers = Model:getNumPlayers()
		boxesPerSide = numPlayers/2
		if math.floor(boxesPerSide) ~= boxesPerSide then
			boxesPerSide = (numPlayers+1)/2
		end

		for i = 1, numPlayers do
			local tableToUse = i%2 == 0 and playerBoxes.leftSide or playerBoxes.rightSide
			table.insert(tableToUse, {playerNdx = i})
		end

		local boxHeight = my.height/boxesPerSide
		local count = 1
		local keysToUse = Model:getPlayers()

		local function placeBoxOnSide(i,x,side)
			local params = {
				ndx = count,
				w = boxWidth,
				h = boxHeight,
				sheet = Model:getRandomSheet(),
				inputKey = keysToUse[count],
				side = side
			}
			local box = PlayerBox:new(params)
			box:init()
			box.x = x
			box.y = (-my.height/2) + (boxHeight/2) + ((i-1) * boxHeight)
			count = count + 1
			view:insert(box)
			table.insert(currentPlayers, box)
		end


		for i = 1, #playerBoxes.leftSide do
			placeBoxOnSide(i ,(-my.width/2) + (boxWidth/2), "left") -- left side!
		end

		for i = 1, #playerBoxes.rightSide do
			placeBoxOnSide(i,(my.width/2) - (boxWidth/2), "right") -- left side!
		end
		

	end

	function view:destroy()
		logViewOut("playerUI")
		for i = 1, #currentPlayers do
			if currentPlayers[i] then
				currentPlayers[i]:destroy()
				currentPlayers[i]=nil
			end
		end	
		cleanGroup(view); view = nil

	end



	return view




end

return PlayerUI
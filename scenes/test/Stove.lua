local Stove = {}

function Stove:new()

	local view = display.newGroup()

	function view:init()
		local lady = Sheets:getAnimatedSprite("Stove", {
			name = "stove",
			start = 1,
			count = 2,
			time = 1000,
			loopCount = 0,
			loopDirection = "forward" -- "bounce"
		})
		lady:setSequence("stove")
		lady:play()
		lady:scale(.35,.35)
		view:insert(lady)
	end



	return view





end


return Stove
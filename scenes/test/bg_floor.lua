local bg_floor = {}

function bg_floor:new()

	local view = display.newGroup()
	local bg_floor_sprite

	function view:init()
		bg_floor_sprite = Sheets:getAnimatedSprite("bg_floor", {
			name = "bg_floor",
			start = 1,
			count = 2,
			time = 2000,
			loopCount = 0,
			loopDirection = "forward" -- "bounce"
		})
		bg_floor_sprite:setSequence("bg_floor")
		bg_floor_sprite:scale(.5,.5)
		view:insert(bg_floor_sprite)
	end

	function view:play()
		bg_floor_sprite:play()
	end




	return view





end


return bg_floor
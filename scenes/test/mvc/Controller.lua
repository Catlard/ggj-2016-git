local Controller = {}

function Controller:new()
	local controller = {}
	local canceler = Canceler:new()
	local gameCoroutine

	function controller:init(payload) -- starting the game

		print("PAYLOAD WAS")
		dump(payload)

		Model:init()
		Model:setPlayers(payload)

		View:init()

		tellViews("startChangingMovableParts")
		controller:gameRoutine()

	end


	function controller:newLeaderAnnouncement(a)
		Model:setLeaderAnnouncement(a)
		tellViews("highlightPart", a)

	end

	local function CoroPerformWithDelay( delay, func, n )
	    local wrapped = coroutine.wrap( function( event )
	        func( event )  -- Do the action...

	        return "cancel"  -- ...then tell the timer to die when we're done.
	    end )

	    local event2  -- Our "shadow" event.

	    return timer.performWithDelay( delay, function( event )
	        event2 = event2 or { source = event.source }  -- On first run, save source...

	        event2.count = event.count  -- ...update these every time.
	        event2.time = event.time

	        local result = wrapped( event2 )  -- Update the coroutine. It will pick up the event on the first run.

	        if result == "cancel" then
	            timer.cancel( event2.source )  -- After func completes, or on a cancel request, kill the timer.
	        end
	    end, n or 0 )
	end

	local function wait ( duration, update )
	    local now = system.getTimer()
	    local ends_at = now + duration

	    while now < ends_at do
	        if update then -- Call any per-frame behavior. Yielding is discouraged, as it throws off our bookkeeping.
	            update()
	        end
	        coroutine.yield()

	        now = system.getTimer()
	    end
	end




	function controller:gameRoutine()
		local godMovesPerRound = Model:getGodMovesPerRound()
		local godRoundTime = Model:getGodRoundTime()
		CoroPerformWithDelay(50, function() 
			print("PLAYERS: " .. Model:getNumPlayers())

			for i = 1, Model:getNumPlayers()-1 do
				print("HELLO I AM A NEW LEVEL")
				tellViews("addMusicIntensity")
				tellViews("updateForRound",Model:getRound())
				tellViews("enablePlayerMovement")
				for i = 1, godMovesPerRound do
					tellViews("chooseGodMove")
					if i == godMovesPerRound then
						tellViews("showGodCountdown")
					end
					wait(godRoundTime)
				end
				tellViews("disablePlayerMovement")

				Model:newRound()
				tellViews("showScore")
				wait(Model:getScoreTime())
				tellViews("hideScore")


				wait(500)
				tellViews("killWorstPlayer")
				
				wait(Model:getDeathTime())
				
				SFX:play({name = "sfx/laght01", volume = .5},{ loops = 0})
				tellViews("cleanGodCountdown")
				
				SFX:play({name = "sfx/gameover", volume = .5},{ loops = 0})

				wait(1000)
				
			end
			tellViews("killGod")
			Model:killGod()
			wait(1000)
			tellViews("playWinMusic")
			tellViews("showWinner")

		end)
	end

	function controller:resetGame()
		Director:changeScene{scene = "scenes.menu.Menu"}
	end



	function controller:destroy()
		tellViews("destroy")
		View:destroy()
		Model:destroy()
		canceler:destroy(); canceler=nil
	end

	return controller
end

return Controller
local Model = {}

function Model:new()
	local model = {}
	
	local numPlayers = 4
	local players
	local currentAnnouncement

	local currRound = 1

	local wPlayer

	local dollSheets = {
		"A", "B", "C", "king"
	}

	function model:getRandomSheet()

		return dollSheets[math.random(#dollSheets)]
	end

	function model:setCurrentWinner(winner)
		wPlayer = winner
		print("SET CURRENT WINNER TO:")
		dump(wPlayer)
	end

	function model:getCurrentWinner()
		return wPlayer
	end


	function model:getNumPlayers()
		print("GOT num players" .. numPlayers)
		return numPlayers
	end

	function model:getGodRoundTime()
		return 5000
	end

	function model:getScoreTime()
		return (({3000, 2500, 2000, 1500}) [currRound]) or 1000
	end

	function model:getGodMovesPerRound()
		return (({4,4,2,2}) [currRound]) or 2
	end

	function model:getDeathTime()
		local v = (({3000,2000,1000}) [currRound]) or 1000
		return v
	end

	function model:getAnnouncementChangeTime()
		return (({3000, 2500, 2000, 1500}) [currRound]) or 1000
	end

	function model:getRound()

		return currRound
	end


	function model:newRound()
		currRound = currRound + 1
	end

	function model:killGod()
		currentGod = nil
	end

	function model:getPlayerScore(partsTable)
		if not currentGod then
			return 100
		end

		local totalScore = 0
		local godParts = currentGod:getPartsTable()
		-- print("HELLO IO AM A PLAYER SCORE")
		for k, v in pairs(partsTable) do
			if k ~= "maxPoints" then
				local godRot = godParts[k].rotation

				playerRot = v.rotation
				totalScore = totalScore + math.abs(godRot-playerRot)
				-- print(totalScore)
			end
		end
		local percentCorrect = math.floor((1-(totalScore/partsTable.maxPoints)) * 100)
		-- print("ERCENT CORRECT WAS;", percentCorrect)
		-- print("_________")
		return percentCorrect
	end



	function model:setCurrentGod(god)
		currentGod = god
	end


	function model:getMovableParts()
		return {
			"lArm",
			"rArm",
			"lLeg",
			"rLeg"
		}
	end

	function model:getPlayers()
		return players
	end

	function model:setLeaderAnnouncement(a)
		currentAnnouncement = a
	end


	function model:getDollSpeed()
		return (({.3, .4, .5, .6}) [currRound]) or .6
	end

	function model:getGodSpeed()
		return ((({.25, .3, .35, .4}) [currRound]) or .4) -.15
	end

	function model:getLeaderAnnouncement()
		return currentAnnouncement
	end


	function model:setPlayers(payload)
		print("SET PLAYERS TO::::::")
		dump(payload)
		players = payload
		local num = 0
		for k, v in pairs(payload) do
			num = num + 1
		end

		print("SET numPlayers to " .. num)
		numPlayers = num
	end

	function model:init()
	end

	function model:destroy()
	end

	return model
end

return Model
local View = {}

function View:new()
	local view = display.newGroup()

	local inBetweenLayer = display.newGroup()
	local winnerLayer = display.newGroup()


	local Doll = require("scenes.test.Doll")
	local PlayerUI = require("scenes.test.PlayerUI"):new()
	local lady = require("scenes.test.Stove"):new()
	local LittlePeople = require("scenes.test.LittlePeople"):new()
	local bg_floor = require("scenes.test.bg_floor"):new()
	local ball = require("scenes.test.DiscoBall"):new()
	local HumanLeader = require("scenes.test.HumanLeader"):new()
	local MusicMixer = require("scenes.test.MusicMixer"):new()

	local godDoll
	Sheets:add("bg_floor", "assets.art.background")
	Sheets:add("Stove", "assets.art.Stove")
	Sheets:add("LittlePeople", "assets.art.LittlePeople")
	Sheets:add("A", "assets.art.characters.memberA")
	Sheets:add("B", "assets.art.characters.memberB")
	Sheets:add("C", "assets.art.characters.memberC")
	Sheets:add("king", "assets.art.characters.memberKing")
	Sheets:add("discoBall", "assets.art.disco_ball")
	Sheets:add("num", "assets.art.num")
	Sheets:add("boom", "assets.art.boomMeat.boomMeat")
	



	local function onTouch(event)
		print(event.phase)
	end

	function view:updateForRound(roundNum)
		if roundNum == 4 then
			bg_floor:play()
			ball.alpha = 1

		elseif roundNum == 3 then
			lady.alpha = 1

		elseif roundNum == 2 then
			LittlePeople.alpha = 1
		end
	end

	function view:init()

		MusicMixer:init()

		Screen:add(view)
		logViewIn("gameView", view)

		local bg = display.newImage("assets/art/bg.png", true)
		view:insert(bg)
		bg:scale(10, 10)

		view:insert(inBetweenLayer)

		bg_floor:init()
		local sc = 1.34
		bg_floor:scale(sc,sc)
		view:insert(bg_floor)

		lady:init()
		lady.x = 150
		lady.y = 50
		view:insert(lady)
		lady.alpha = 0

		LittlePeople:init()
		LittlePeople.x = -100
		LittlePeople.y = 50
		view:insert(LittlePeople)
		LittlePeople.alpha = 0


		godDoll = Doll:new {sheet = Model:getRandomSheet(), speed = Model:getGodSpeed()}
		godDoll:init()
		Model:setCurrentGod(godDoll)
		view:insert(godDoll)
		godDoll.y = -50


		view:insert(winnerLayer)

		PlayerUI:init()
		view:insert(PlayerUI)

		ball:init()
		ball.y = 60
		ball.alpha = 0
		

		HumanLeader:init()
		view:insert(HumanLeader)
		HumanLeader.y = (-my.height/2) + 30



	end

	function view:killGod()
		godDoll:getEaten(function()
			godDoll:destroy(); godDoll = nil
		end)
	end

	function view:showWinner()
		local winner = Model:getCurrentWinner()

		local r = display.newRoundedRect(0,-100, 400, 75, 10)
		local c = winner:getCustomColor()
		
		r:setFillColor(unpack(c))
		r.strokeWidth = 5
		winnerLayer:insert(r)
		local winningKey = winner:getCustomDescriptor()

		

		local options = {
		    text =  winningKey .. " wins!", --params.ndx,     
		    fontSize = 35,
		    align = "center"
		}
		local t = display.newText(options)
		t.y = -100

		winnerLayer:insert(t)



	end


	--stop god from moving as well
	function view:disablePlayerMovement()
		godDoll:setCanMove(false)
	end

	function view:enablePlayerMovement()
		godDoll:setCanMove(true)
	end


	function view:chooseGodMove()
		godDoll:setPartsToMove(1)
	end	

	function view:destroy()
		logViewOut("gameView")
		cleanGroup(view); view = nil
	end

	function view:showGodCountdown()
		godDoll:countDownFrom(5)
	end	

	function view:cleanGodCountdown()
		godDoll:cleanCountdown()
	end	

	return view
end

return View

--[[JON -- YOU CAN EDIT THIS FILE AS YOU WISH, AND SEND BACK
TO ME WHEN YOU'RE HAPPY WITH THE SPECIFIC NUMBERS/FILES IN PLACE.
I DESCRIBED EACH LINE WITH SOME COMMENTS -- SO YOU CAN SEE
WHAT EACH ONE MEANS. YOU CAN MAKE A COPY OF THIS FILE AND DELETE 
THE COMMENTS IF IT MAKES THE TABLES (THINGS WITHIN "{" AND "}" ) 
EASIER FOR YOU TO READ. 

Note: One possibly tricky thing here is the way commas work.
Forgetting a comma could make this file not compile properly. 
Basically, commas seperate things in a table (a list, things
between {}. You don't need a comma after the last thing in a
table, but it won't break anything.]]

return {
	--[[menuButtonHit, a category that I, the programmer, may
	call for in the course of the game running. you can invent
	your own sound categories, and add whatever you want to this
	file, but they won't be used unless they are called from
	the game code.]]
	menuButtonHit = {  
		--[[name of the sound file, WITHOUT extension (like .wav).
		this is required.]]
		name = "menuButtonHit",
		--[[a range of acceptable pitches. i can 
		use this in code for whatever, if it's 
		a sound that will change pitch. this is optional.]]
		pitch = {.8, 1},
		--[[the volume that this sound will be played at
		in the game. this is optional. 1 is max volume.]]
		volume = 1 -- or {.5, 1} to randomly select between that range
	},

	--[[pieEat is a category. if there are more than 
	one in a category, the game is programmed to randomly 
	select one.]]
	pieEat = { 
		--[[pieEat1 is the first sound that might randomly be
		selected.]]
		{
			name = "pieEat1", 
			pitch = {.5, 1}, 
			volume = .5
		},
		--[[here it is without comments -- this 
		is pieEat2, now. this sound also has a 50% chance
		of firing when the game calls for a "pieEat sound."]]
		{
			name = "spicyMeatball", -- could be any name, but probably for your own sanity you won't name sounds things like this unless they're used for multiple things or something.
			pitch = {.7, 1},
			volume = 1
		}

	},
	
	--[[ another category. this is the super short 
	version, minimum customization. just one line!
	easy, no?]]
	newPie = { name = "newPie", volume = .25 },

	--[[that's it! music is done in a the same way
	as regular sounds. you don't have to have all the
	returns -- you can do it on one line, even with
	the pitch/volume customization. enjoy!]]
	menuMusic = { name = "menuMusic", volume = .3},
	gameOver = { name = "gameOver", pitch = {.8,1}},
	pieEat = {name = "pieEat", pitch = {.5, 1}, volume = .5},


	wrongGet = { name = "pieEat", pitch = {.5, 1}, volume = 1},

	--[[ These are the noises that play when the pie 
	is showing you the solution. They play in a loop--
	1,2,3,4,1,2....]]
	solutionBoop = { 
		{name = "boop1", volume = .5},
		{name = "boop2", volume = .5},
		{name = "boop3", volume = .5},
		{name = "boop4", volume = .5},
	}

}
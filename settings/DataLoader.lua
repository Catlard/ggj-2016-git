local DataLoader = {}

--takes the same params, that's how it reads file names. synchronous.
local function readIntoMemory(params)
	for i = 1, #params.sheets do
		local fName = params.sheets[i].title
		my.configData[fName] = KittenIO:fetchTable {
			location = "settings/data/",
	        fileName = fName
		}
	end
end

function DataLoader:finish(startup, params)
	readIntoMemory(params)
	startup:resume("DataLoader")
end

--loads from google if necessary. params = my.startupFetch, atow
function DataLoader:init(startup, params)
	if FETCH_FROM_GOOGLE then
		params.callback = function(data)
	        for fileName, table in pairs(data) do 
	            KittenIO:saveTable{
	                location = "settings/data/",
	                fileName = fileName,
	                table =  table,
	            }
	        end
	        self:finish(startup, params)
	    end
	  	GoogleDocFetcher:fetch(params)
	else
		self:finish(startup, params)
	end
end






return DataLoader
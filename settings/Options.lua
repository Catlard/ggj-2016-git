----------------------------------
-------- Gameplay changes --------
----------------------------------

-- MUTE = true -- all sound off.
-- CHEATING_PUZZLE = true -- game doesn't care what order you press the slices in.
-- CHEATING_DEATH = true -- game doesn't care if you run out of time.
-- FETCH_FROM_GOOGLE = true

----------------------------------
-------- Debug    changes --------
----------------------------------

-- CONSOLE = true -- console on or off?
-- DEBUG = true -- shows the debug menu.
-- DEBUG_GOOGLE_FETCHES = true
-- DEBUG_KITTEN_IO = true
-- DEBUG_MVC_CONTEXT = true